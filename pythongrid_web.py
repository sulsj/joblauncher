#! /usr/bin/env python
# -*- coding: utf-8 -*-
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Modified (M) 2012-2013 Seung-Jin Sul
# Written (W) 2011 Christian Widmer
# Copyright (C) 2011 Max-Planck-Society
"""

simple web front-end for pythongrid
@author Christian Widmer

"""

import cherrypy
import sys

from pythongrid import send_zmq_msg

#------------------------------------------------------------------------------
class WelcomePage:
#------------------------------------------------------------------------------

    title = 'JobLauncher Status Report'
    
    def header(self):
        return '''
                <html>
                <head>
                    <title>%s</title>
                </head>
                <body>
                <h2>%s</h2>
            ''' % (self.title, self.title)
    
    def footer(self):
        return '''
                </body>
                </html>
            '''
        
    def index(self):

        return self.header() + '''
            <form action="list_jobs" method="GET">
            Address of JobLauncher session: <br />
            <input type="text" name="address" /><br /><br />
            <input type="submit" />
            </form>''' + self.footer()
    index.exposed = True


    def list_jobs(self, address):
        """
        display list of jobs
        self.num_resubmits = 0
        self.cause_of_death = ""
        self.jobid = -1
        self.host_name = ""
        self.timestamp = None
        self.heart_beat = None
        self.exception = None
        """

        job_id = -1
        self.jobs = send_zmq_msg(job_id, "get_jobs", "", address, "cherry")

        out_html = '''
            <form action="list_jobs" method="GET">
            Address of JobLauncher session: <br />
            <input type="text" name="address" /><br /><br />
            <input type="submit" /><br><br><br>
            </form>

            <form action="view_job" method="GET">
            <table border="1">
            <tr><td>sge job id</td><td>job done</td><td>cause of death</td><td>num resubmissions</td><tr>
            '''

        
        for job in self.jobs:
            out_html += "<tr><td><a href='/view_job?address=%s&job_id=%s'>%s</td>" % (address, str(job.name), str(job.jobid))
            if job.ret == None and len(job.qw_end) > 0:
                out_html += "<td>%s</td>" % ("Running")
            elif job.ret!=None and job.cause_of_death == "usercmd_error":
                out_html += "<td>%s</td>" % ("Failed")               
            else:
                out_html += "<td>%s</td>" % (str(job.ret!=None))
            out_html += "<td>%s</td>" % (job.cause_of_death)
            out_html += "<td>%s</td>" % (str(job.num_total_resubmits))

            out_html += "</tr>"

        out_html += "</table></form>"

        return self.header() + out_html + self.footer()
    list_jobs.exposed = True


    def view_job(self, address, job_id):
        """
        display list of jobs
        self.num_resubmits = 0
        self.cause_of_death = ""
        self.jobid = -1
        self.host_name = ""
        self.timestamp = None
        self.heart_beat = None
        self.exception = None
        """

        job = send_zmq_msg(job_id, "get_job", "", address, "cherry")

        out_html = ""
        
        details = job_to_html(job)

        out_html += details


        return out_html
    view_job.exposed = True


#------------------------------------------------------------------------------
def job_to_html(job):
#------------------------------------------------------------------------------
    """
    display job as html
    """

    # compose error message
    body_text = ""

    #body_text += "job " + str(job.name) + "\n<br>"
    #body_text += "last timestamp: " + str(job.timestamp) + "\n<br>"
    #body_text += "num_resubmits: " + str(job.num_resubmits) + "\n<br>"
    #body_text += "cause_of_death: " + str(job.cause_of_death) + "\n<br>"

    body_text += "job: " + str(job.name) + "\n<br>"
    body_text += "last timestamp: " + str(job.timestamp) + "\n<br>"
    body_text += "num_resubmits: " + str(job.num_total_resubmits) + "\n<br>"
    body_text += "num_resubmits (qsub): " + str(job.num_resubmits_qsub) + "\n<br>"
    body_text += "num_resubmits (mem): " + str(job.num_resubmits_mem) + "\n<br>"
    body_text += "num_resubmits (wallclocktime): " + str(job.num_resubmits_wallclock) + "\n<br>"
    body_text += "num_resubmits (output): " + str(job.num_resubmits_output) + "\n<br>"
    body_text += "cause_of_death: " + str(job.cause_of_death) + "\n<br>"
    
    body_text += "Queue wait time: \n<br>"
    for i in range(min(len(job.qw_start), len(job.qw_end))):
        body_text += str(job.qw_end[i] - job.qw_start[i]) + "\n<br>"
    
    #if job.heart_beat:
    #    body_text += "last memory usage: " + str(job.heart_beat["memory"]) + "\n<br>"
    #    body_text += "last cpu load: " + str(job.heart_beat["cpu_load"]) + "\n<br>"
        
    #if job.heart_beat:
    #    body_text += "Python resource consumption ---\n<br>"
    #    body_text += "--- last memory usage: " + str(job.heart_beat["memory"]) + "\n<br>"
    #    body_text += "--- last cpu load: " + str(job.heart_beat["cpu_load"]) + "\n<br>"
    #    body_text += "--- last runtime: " + str(job.heart_beat["runtime"]) + " seconds\n<br>"
        
    if job.proc_mon:
        body_text += "User job resource consumption ---\n<br>"
        body_text += "--- last memory usage: " + str(job.proc_mon["memory"]) + "\n<br>"
        body_text += "--- last cpu load: " + str(job.proc_mon["cpu_load"]) + "\n<br>"
        body_text += "--- last runtime: " + str(job.proc_mon["runtime"]) + " seconds\n<br>"
        body_text += "\n<br>"
        body_text += "User command's mem usage: %s" % (job.track_procmon_mem) + "\n<br>"
        body_text += "User command's cpu usage: %s" % (job.track_procmon_cpu) + "\n<br>"
        body_text += "User command's runtime: %s seconds" % (job.track_procmon_runtime) + "\n<br>\n<br>"
    #body_text += "requested memory: " + str(job.h_vmem) + "\n<br>"
    #body_text += "host: <a href='http://sambesi.kyb.local/ganglia/?c=Three Towers&h=%s&m=load_one&r=hour&s=descending&hc=5&mc=2'>%s</a><br><br>\n\n" % (job.host_name, job.host_name)
    
    body_text += "requested memory (ram.c): " + str(job.ram_c) + "\n<br>"
    body_text += "requested memory (h_vmem): " + str(job.h_vmem) + "\n<br>"
    body_text += "compute node name: " + str(job.host_name) + "\n\n<br>"
    
    
    if isinstance(job.ret, Exception):
        body_text += "job encountered exception: " + str(job.ret) + "\n<br>"
        body_text += "stacktrace: " + str(job.exception) + "\n<br>\n<br>"
    
    
    # attach log file
    if job.heart_beat:
        log_file = open(job.heart_beat["log_file"], "r")
        log_file_attachement = log_file.read().replace("\n", "<br>\n")
        log_file.close()

        body_text += "<br><br><br>" + log_file_attachement


    return body_text


class HelloWorld(object):
    def index(self):
        return "Hello World!"
    index.exposed = True


cherrypy.tree.mount(WelcomePage())

if __name__ == '__main__':
    print sys.argv
    import os.path
    thisdir = os.path.dirname(__file__)
    response = cherrypy.response
    #response.headers['Content-Type'] = 'application/json'
    #response.body = encoder.iterencode(response.body)
    #cherrypy.quickstart(config=os.path.join(thisdir, 'pythongrid_web.conf'))
    #cherrypy.quickstart(HelloWorld(), config=os.path.join(thisdir, 'pythongrid_web.conf'))
    cherrypy.quickstart(WelcomePage(), config=os.path.join(thisdir, 'pythongrid_web.conf'))



# EOF

