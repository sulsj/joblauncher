#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012-2013 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""
Subclassing Job for NERSC clusters

"""

__version__ = "2.3"

import math
from pythongrid import *

import logging
logger = logging.getLogger('root')

from Utils.OsUtility import convHhmmssToSeconds, convSecToHhmmss

"""
Specialization of generic Job that provides an interface to
the Genepool.
"""
#------------------------------------------------------------------------------
class NerscJob(Job):
#------------------------------------------------------------------------------

    def __init__(self, f, args, kwlist={}, param=None, cleanup=True):
        Job.__init__(self, f, args, kwlist, param=param, cleanup=cleanup)
        
        self.name = ""
        self.h_vmem =  ""
        self.ram_c = ""
        self.h_rt = ""
        #self.array_job = "" ## reserved for implementing array job
        self.sge_param = ""
        self.user_cmd = ""
        
        ## dealing with output file checking
        self.output_file = [] # for multiple output files
        self.error_code = ""
        
        ## additional fields for robustness
        self.num_total_resubmits = 0
        
        ## sulsj
        self.num_resubmits_qsub = 0
        self.num_resubmits_mem = 0
        self.num_resubmits_wallclock = 0
        self.num_resubmits_output = 0
        
        self.cause_of_death = ""
        self.jobid = -1
        self.host_name = ""
        self.timestamp = None
        self.qw_start = []
        self.qw_end = []
        self.heart_beat = None
        self.proc_mon = None
        self.exception = None
        
        ## fields to track mem/cpu-usage
        self.track_mem = []
        self.track_cpu = []
        self.track_runtime = "" # just keep the final runtime value in sec
        
        ## This is for run_something procmon
        self.track_procmon_mem = []
        self.track_procmon_cpu = []
        self.track_procmon_runtime = ""

        ## black and white lists
        self.white_list = ""
        ## working directory
        self.working_dir = os.getcwd()


    def getNativeSpecification(self):
        """
        define python-style getter
        """

        ret = ""

        if (self.name != ""):
            ret = ret + " -N " + str(self.name)
        ##
        ## sulsj
        ## To do: supporting array job
        ##
        #if (self.array_job != ""):
            #ret = ret + " -t " + str(self.array_job)
            #CFG['JOBARRAY'] = 1
        if (self.h_vmem != ""):
            ret = ret + " -l " + "h_vmem=" + str(self.h_vmem)
        
        ## To do
        #import re
        #if (self.h_vmem != ""):
        #    h_mem = re.sub("(\d+)\.?\d*(\w)", "\g<1>\g<2>", self.h_vmem)
        #    if h_mem != self.h_vmem:
        #        logger.warming("Warning:", self.h_vmem, "replaced by", h_mem, " for native spec!")
        #    ret = ret + " -l " + "h_vmem" + "=" + str(self.h_vmem)
        
        if (self.ram_c != ""):
            ret = ret + " -l " + "ram.c=" + str(self.ram_c)
        if (self.h_rt != ""):
            ret = ret + " -l " + "h_rt=" + str(self.h_rt)
        if (self.sge_param != ""):
            ret = ret + " " + str(self.sge_param)
        
        return ret


    def setNativeSpecification(self, x):
        """
        define python-style setter
        
        @param x: nativeSpecification string to be set
        @type x: string
        """

        self.__nativeSpecification = x

    nativeSpecification = property(getNativeSpecification, setNativeSpecification)

    
    def checkOutOfMem(self):
        """
        checks if job is out of memory according to requested resources and the
        last heart-beat
        """

        logger.debug("Checking out_of_memory")

        ## compare memory consumption from
        ## last heart-beat to requested memory
        if self.heart_beat != None:
            ##
            ## To do: Why ?
            ## self.heart_beat["vms"] != float(self.proc_mon["vms"])
            ##
            unit = self.h_vmem[-1]
            requestedMemMB = float(self.h_vmem[0:-1])
            #usedMemMB = float(self.heart_beat["vms"])
            usedMemMB = float(self.proc_mon["vms"])

            if (unit == "G" or unit == "g"):
                requestedMemMB *= 1000

            ## 10% more mem, at least 1G more than last heart-beat
            #memUpperLimit = max(float(self.heart_beat["memory"]) + 1000,
                                #float(self.heart_beat["memory"]) * CFG['MEM_THRESHOLD'])
            #memUpperLimit = max(float(self.heart_beat["vms"]) + 1000,
                                #float(self.heart_beat["vms"]) * CFG['MEM_THRESHOLD'])
            memUpperLimit = max(usedMemMB + 1000,
                                usedMemMB * CFG['MEM_THRESHOLD'])
            
            ## Instead compare (cur_mem_usage)*1.20 with requestedMemMB 
            #memUpperLimit = float(self.heart_beat["memory"]) * CFG['MEM_THRESHOLD']
            #memUpperLimit = float(self.heart_beat["vms"]) * CFG['MEM_THRESHOLD']
            memUpperLimit = usedMemMB * CFG['MEM_THRESHOLD']
           
            logger.debug("heart_beat data: %s" % (self.heart_beat))
            logger.debug("proc_mon data: %s" % (self.proc_mon))
           
            logger.debug("Memory used : upperbound : requested = %s,%s : %s : %s" \
                         #% (self.heart_beat["memory"],
                         #% (self.heart_beat["vms"],
                            #self.heart_beat["rss"],
                         % (self.proc_mon["vms"],
                            self.proc_mon["rss"],
                            memUpperLimit,
                            requestedMemMB))

            ## if we are within memory limit
            if memUpperLimit > requestedMemMB:
                logger.debug("Out-of-mem detected! used:requested = %s:%s" % (memUpperLimit, requestedMemMB))
                return True

        ## if we have a memory exception
        if isinstance(self.ret, MemoryError):
            logger.debug("Memory system exception was detected!")
            return True
        
        ## Sometimes out-of-memory causes a System Error
        if isinstance(self.ret, SystemError):
            ## we look for memory keyword in exception detail (heuristic but useful)
            exceptionDetail = str(self.exception).lower()

            if exceptionDetail.find("memory") != -1:
                logger.debug("System error caused by out-of-mem was detected!")
                return True

        ## other problem
        return False
        

    def increaseMemRequest(self, factor=2.0):
        """
        increases requested memory by a certain factor
        Note: ram.c and h_vmem are increased by the same factor
        """
        
        assert(type(factor)==float)

        if self.h_vmem != "":
            ramcUnit = self.ram_c[-1]
            hvmemUnit = self.h_vmem[-1]
            prevRamcMB = float(self.ram_c[0:-1])
            prevHvmemMB = float(self.h_vmem[0:-1])
            
            #requestedMemMB = int(self.h_vmem[0:-1])
            self.ram_c = "%d%s" % (math.ceil(factor * prevRamcMB), ramcUnit)
            self.h_vmem = "%d%s" % (math.ceil(factor * prevHvmemMB), hvmemUnit)
            logger.info("=====================================")
            logger.info("memory for job '%s' increased from ram.c=%s%s, h_vmem=%s%s to ram.c=%s, h_vmem=%s" % (self.name, prevRamcMB, ramcUnit, prevHvmemMB, hvmemUnit, self.ram_c, self.h_vmem))
            
        return self

    ## sulsj
    """
    increase the wallclocktime 
    """
    
    def checkOutOfWallclocktime(self):
        """
        checks if job is out of wallclocktime according to requested resources
        and the last heart-beat
        """
        
        logger.debug("Checking out_of_wallclocktime")
        
        if self.heart_beat != None:
            currRuntime = self.heart_beat["runtime"]
            requestedWalltime = self.h_rt
            
            if int(currRuntime) >= int(convHhmmssToSeconds(requestedWalltime) * CFG['WALLCLOCKTIME_THRESH']):
                logger.debug("out-of-wallclock detected!")
                return True
            
        return False
    
    def increaseWalltimeRequest(self, factor=2.0):
        """
        increases requested wallclocktime by a certain factor

        """
        assert(type(factor)==float)

        if self.h_rt != "":
            hrtSec = convHhmmssToSeconds(self.h_rt)
            increasedHrtSec = hrtSec * factor
            self.h_rt = convSecToHhmmss(increasedHrtSec)
            logger.info("=====================================")
            logger.info( "h_rt for job '%s' increased from %s to %s" % (self.name, convSecToHhmmss(hrtSec), self.h_rt))

        return self


# EOF