qsub-wrapper
============

A robust qsub wrapper for general purpose qsub replacement.

This targets at developing a backbone implementation of workflow management
system. It provides users with an unified way to submit their jobs and to check
output files for synchronization. At JGI, there are many different ways to do
that.

Goal:
 
- Provide a standard way to dealing with issues like invalid job ID, out-of-memory,
  out-of-wallclocktime, node failure, etc.
- Develop a framework for the implementation of checkpointing and workflow
  management.
  
  

  
