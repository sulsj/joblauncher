#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Modified (M) 2012-2013 Seung-Jin Sul
# Written (W) 2008-2012 Christian Widmer
# Written (W) 2008-2010 Cheng Soon Ong
# Copyright (C) 2008-2012 Max-Planck-Society

"""
pythongrid provides a high level front-end to DRMAA-python.

This module provides wrappers that simplify submission and collection of jobs,
in a more 'pythonic' fashion.

We use pyZMQ to provide a heart beat feature that allows close monitoring
of submitted jobs and take appropriate action in case of failure.

"""

__version__ = "2.3"

import sys
import os
import os.path
import inspect
import getopt
import time
import random
import traceback
import zmq
import socket
import uuid
import getpass
import datetime

from joblauncher_cfg import CFG
from Utils.MsgCompress import *
from Utils.OsUtility import *
from Utils.ResourceUsage import *

import subprocess

import logging
logger = logging.getLogger('root')

## To kill web server
#import signal
#import failure
#import smtplib
#from email.mime.multipart import MIMEMultipart
#from email.mime.text import MIMEText
#from emailsil.mime.image import MIMEImage

jp = os.path.join

DRMAA_PRESENT = False
MULTIPROCESSING_PRESENT = False
#MATPLOTLIB_PRESENT = False
#CHERRYPY_PRESENT = False

try:
    import drmaa
    DRMAA_PRESENT = True
except Exception, detail:
    print "Error importing drmaa. Only local multi-threading supported."
    print "Please check your installation."
    print detail

try:
    import multiprocessing
    MULTIPROCESSING_PRESENT = True
except Exception, detail:
    print "Error importing multiprocessing. Local computing limited to one CPU."
    print "Please install python2.6 or the backport of the multiprocessing package"
    print detail

if CFG["CREATE_PLOTS"]:
     try:
         import pylab
         MATPLOTLIB_PRESENT = True
     except Exception, detail:
         print "Error importing pylab. Not plots will be created in debugging emails."
         print "Please check your installation."
         print detail

if CFG["USE_CHERRYPY"]:
     try:
         import cherrypy
         CHERRYPY_PRESENT = True
     except Exception, detail:
         print "Error importing cherrypy. Web-based monitoring will be disabled."
         print "Please check your installation."
         print detail


#-------------------------------------------------------------------------------
class Job(object):
#-------------------------------------------------------------------------------
    """
    Central entity that wraps a function and its data. Basically,
    a job consists of a function, its argument list, its
    keyword list and a field "ret" which is filled, when
    the execute method gets called
    """

    def __init__(self, f, args, kwlist={}, param=None, cleanup=True):
        """
        constructor

        @param f: a function, which should be executed.
        @type f: function
        @param args: argument list of function f
        @type args: list
        @param kwlist: dictionary of keyword arguments
        @type kwlist: dict
        @param cleanup: flag that determines the cleanup of input and log file
        @type cleanup: boolean
        """

        self.__set_function(f)
        self.args = args
        self.kwlist = kwlist
        self.cleanup = cleanup
        self.ret = None
        self.nativeSpecification = ""
        self.exception = None
        self.environment = None
        self.replace_env = False
        self.working_dir = None

        self.log_stdout_fn = ""
        self.log_stderr_fn = ""

        if param != None:
            self.__set_parameters(param)

        self.name = 'pg_' + str(uuid.uuid1())
        self.jobid = ""


    def __set_function(self, f):
        """
        setter for function that carefully takes care of
        namespace, avoiding __main__ as a module
        """

        m = inspect.getmodule(f)

        ## if module is not __main__, all is good
        if m.__name__ != "__main__":
            self.f = f

        else:
            ## determine real module name
            mn = m.__file__.split("/")[-1].replace(".pyc","").replace(".py", "")

            ## make sure module is present
            __import__(mn)

            ## get module
            mod = sys.modules[mn]

            ## set function from module
            self.f = mod.__getattribute__(f.__name__)


    def __set_parameters(self, param):
        """
        method to set parameters from dict
        """

        assert(param!=None)

        for (key, value) in param.items():
            setattr(self, key, value)

        return self


    def execute(self):
        """
        Executes function f with given arguments and writes return value to
        field ret. If an exception is encountered during execution, ret will
        remain empty and the exception will be written to the exception field of
        the Job object. Input data is removed after execution to save space.
        """

        try:
            self.ret = apply(self.f, self.args, self.kwlist)
            logger.debug("returncode = %s" % (self.ret))
        except Exception, e:
            print "ERROR: Exception encountered"
            print "type:", str(type(e))
            print "line number:", sys.exc_info()[2].tb_lineno
            print e
            print "========="
            self.exception = traceback.format_exc()
            self.ret = e

            print self.exception
            traceback.print_exc(file=sys.stdout)


#-------------------------------------------------------------------------------
def submitJobs(jobs, home_address, white_list=""):
#-------------------------------------------------------------------------------
    """
    Method used to send a list of jobs onto the cluster.
    @param jobs: list of jobs to be executed
    @type jobs: list<Job>
    """

    session = drmaa.Session()
    session.initialize()
    jobids = []
    jobNo = 1

    for job in jobs:
        logger.info('Job #: %d' % (jobNo))

        ## set job white list
        job.white_list = white_list

        ## remember address of submission host
        job.home_address = home_address

        ## append jobs
        before = datetime.datetime.now()
        jobid = appendJobToSession(session, job)
        logger.debug('time to submit: %s' % (str(datetime.datetime.now() - before)))

        jobids.append(jobid)
        jobNo += 1

    sid = session.contact
    session.exit()

    return (sid, jobids)


#-------------------------------------------------------------------------------
def appendJobToSession(session, job):
#-------------------------------------------------------------------------------
    """
    For an active session, append new job
    based on information stored in job object

    side-effects:
    - job.jobid set to jobid determined by grid-engine
    - job.log_stdout_fn set to std::out file
    - job.log_stderr_fn set to std::err file
    """

    jt = session.createJobTemplate()

    ## fetch env vars from shell
    shell_env = os.environ

    if job.environment and job.replace_env:
        ## only consider defined env vars
        jt.jobEnvironment = job.environment

    elif job.environment and not job.replace_env:
        ## replace env var from shell with defined env vars
        env = shell_env
        env.update(job.environment)
        jt.jobEnvironment = env

    else:
        ## only consider env vars from shell
        jt.jobEnvironment = shell_env

    #print jt.jobEnvironment
    jt.remoteCommand = os.path.expanduser(CFG['PYGRID'])

    ######################################################
    ## This args will be passed to run_job()
    jt.args = [job.name, job.home_address, CFG['TEMPDIR']]
    ######################################################
    #jt.joinFiles = True
    jt.nativeSpecification = job.nativeSpecification
    jt.outputPath = ":" + os.path.expanduser(CFG['TEMPDIR'])
    jt.errorPath = ":" + os.path.expanduser(CFG['TEMPDIR'])

    ##########################
    # jobid checking and retry
    ##########################
    jobid = 0
    wait_sec = CFG["QSUB_RETRY_SLEEP_TIME"]
    success = False
    while not success and job.num_resubmits_qsub < CFG["NUM_MAX_RESUBMITS_QSUB"]:
        jobid = session.runJob(jt)
        
        if (jobid <= 0 or jobid == None):
            job.num_resubmits_qsub += 1
            add_sec = random.randint(0, 5)
            wait_sec = (wait_sec * CFG["QSUB_RETRY_SLEEP_INCREASE"]) + add_sec
            logger.critical("ERROR: Qsub failed for '%s'. Will retry after %f sec" % (job.name, wait_sec))
            time.sleep(wait_sec)

            ## To do: runBulkJobs() for job array
            ##
            jobid = session.runJob(jt)
        else:
            success = True

    if not success:
        logger.critical('\nERROR: Your job %s was not submitted successfully.' % (job.name))

        # To do: Do I have to terminate all other jobs qsub'd or running?
        # try:
        #    session.control(job.jobid, drmaa.JobControlAction.TERMINATE)
        #    print "zombie job killed"
        #except Exception, detail:
        #    print "could not kill job with SGE id", job.jobid
        #    print detail

        sys.exit(1)

    job.qw_start.append(datetime.datetime.now())

    ## set job fields that depend on the jobid assigned by grid engine
    job.jobid = jobid
    job.log_stdout_fn = (os.path.expanduser(CFG['TEMPDIR']) + "/" + job.name + '.o' + jobid)
    job.log_stderr_fn = (os.path.expanduser(CFG['TEMPDIR']) + "/" + job.name + '.e' + jobid)

    logger.info("=====================================")
    logger.info( "Your job '%s' has successfully been submitted with jobid '%s'" % (job.name, jobid))
    logger.info( "User command: %s" % (job.user_cmd))
    logger.info( "sge param: name=%s, ram.c=%s, h_vmem=%s, h_rt=%s, outfiles=%s, param=%s" % (job.name, job.ram_c, job.h_vmem, job.h_rt, job.output_file, job.sge_param))
    logger.info( "stdout: %s" % (job.log_stdout_fn))
    logger.info( "stderr: %s" % (job.log_stderr_fn))

    session.deleteJobTemplate(jt)


    return jobid


#-------------------------------------------------------------------------------
def collectJobs(sid, jobids, joblist, wait=False):
#-------------------------------------------------------------------------------
    """
    Collect the results from the jobids, returns a list of Jobs

    @param sid: session identifier
    @type sid: string returned by cluster
    @param jobids: list of job identifiers returned by the cluster
    @type jobids: list of strings
    @param wait: Wait for jobs to finish?
    @type wait: Boolean, defaults to False
    """

    for ix in xrange(len(jobids)):
        assert(jobids[ix] == joblist[ix].jobid)

    s = drmaa.Session()
    s.initialize(sid)

    if wait: drmaaWait = drmaa.Session.TIMEOUT_WAIT_FOREVER
    else: drmaaWait = drmaa.Session.TIMEOUT_NO_WAIT

    s.synchronize(jobids, drmaaWait, True)
    print "Success: all jobs finished"
    s.exit()

    ## attempt to collect results
    retJobs = []
    for ix, job in enumerate(joblist):
        log_stdout_fn = (os.path.expanduser(CFG['TEMPDIR']) + "/" + job.name + '.o' + jobids[ix])
        log_stderr_fn = (os.path.expanduser(CFG['TEMPDIR']) + "/" + job.name + '.e' + jobids[ix])

        try:
            retJob = load(job.outputfile)
            assert(retJob.name == job.name)
            retJobs.append(retJob)

            ## print exceptions
            if retJob.exception != None:
                print str(type(retJob.exception))
                print "ERROR: Exception encountered in job with log file:"
                print log_stdout_fn
                print retJob.exception

            ## remove files
            elif retJob.cleanup:
                print "Cleaning up:", job.outputfile
                os.remove(job.outputfile)

                if retJob != None:
                    print "Cleaning up:", log_stdout_fn
                    os.remove(log_stdout_fn)

                    print "Cleaning up:", log_stderr_fn
                    #os.remove(log_stderr_fn)


        except Exception, detail:
            print "Error while unpickling file: " + job.outputfile
            print "this could caused by a problem with the cluster environment, imports or environment variables"
            print "check log files for more information: "
            print "stdout:", log_stdout_fn
            print "stderr:", log_stderr_fn

            print detail

    return retJobs


#-------------------------------------------------------------------------------
def processJobs(jobs, local=False, maxNumThreads=1):
#-------------------------------------------------------------------------------
    """
    Director function to decide whether to run on the cluster or locally
    local: local or cluster processing
    """
    
    if (not local and DRMAA_PRESENT):
        ## get list of trusted nodes
        white_list = CFG['WHITELIST']

        ## initialize checker to get port number
        checker = StatusCheckerZMQ()

        ## get interface and port
        home_address = checker.home_address

        ## jobid field is attached to each job object
        (sid, jobids) = submitJobs(jobs, home_address, white_list)

        ## handling of inputs, outputs and heartbeats
        checker.check(sid, jobs)

        return jobs

    elif (not local and not DRMAA_PRESENT):
        logger.critical('ERROR: failed to import drmaa.')
        sys.exit(1)
    #    return  _process_jobs_locally(jobs, maxNumThreads=maxNumThreads)
    #
    #else:
    #    return  _process_jobs_locally(jobs, maxNumThreads=maxNumThreads)


# def get_status(sid, jobids):
#     """
#     Get the status of all jobs in jobids.
#     Returns True if all jobs are finished.
#
#     Using the state-aware StatusChecker now, this
#     function maintains the previous
#     interface.
#     """
#
#     checkIfAlive = StatusChecker(sid, jobids)
#     return checker.check()


#-------------------------------------------------------------------------------
class StatusCheckerZMQ(object):
#-------------------------------------------------------------------------------
    """
    switched to next-generation cluster computing :D
    """

    def __init__(self):
        """
        set up socket
        """

        context = zmq.Context()
        self.socket = context.socket(zmq.REP)

        self.host_name = socket.gethostname()
        self.ip_address = socket.gethostbyname(self.host_name)
        self.interface = "tcp://%s" % (self.ip_address)

        ## bind to random port and remember it
        self.port = self.socket.bind_to_random_port(self.interface, max_tries=10)
        assert (self.port > 0)
        self.home_address = "%s:%i" % (self.interface, self.port)

        logger.info("Setting up ZMQ connection using %s" % (self.home_address))

        self.cherry_pid = -1

        ## To do start web-based monitorsing process correctly
        if False and CHERRYPY_PRESENT:
            logger.info("Starting web interface")
            #subprocess.Popen("python pythongrid_web.py " + self.home_address)
            cmd = "python pythongrid_web.py " + self.home_address
            p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
            self.cherry_pid = p.pid

        ## uninitialized field (set in check method)
        self.jobs = []
        self.jobids = []
        self.session_id = -1
        self.jobid_to_job = {}


    def __del__(self):
        """
        clean up open socket
        """

        self.socket.close()


    def check(self, session_id, jobs):
        """
        serves input and output data
        """

        ## save list of jobs
        self.jobs = jobs

        ## keep track of DRMAA session_id (for resubmissions)
        self.session_id = session_id

        ## save useful mapping
        self.jobid_to_job = {}
        for job in jobs:
            self.jobid_to_job[job.name] = job

        ## determines in which interval to check if jobs are alive
        local_heart = multiprocessing.Process(target=heart_beat,
                                              args=(-1,
                                                    self.home_address,
                                                    "heart_beat",
                                                    "local",
                                                    -1,
                                                    "",
                                                    CFG["CHECK_FREQUENCY"]))
        local_heart.start()

        logger.info("=====================================")
        logger.info( " Using ZMQ layer to keep track of jobs")
        logger.info( " Waiting for allocation...")
        logger.info("=====================================")

        ## main loop ###########################################################
        while not self.checkAllDone():

            msgStr = self.socket.recv()
            msg = zloads(msgStr)
            #print "[ZMQ in check()] recv'd = '" + str(msg) + "' by home node"

            return_msg = ""
            job_id = msg["job_id"]

            ## only if it's not the local beat
            if job_id != -1: # job is in running state
                job = self.jobid_to_job[job_id]

                if msg["command"] == "usercmd_error":
                    logger.critical("=====================================")
                    logger.critical("ERROR: usercmd_error catched")
                    return_msg = "usercmd_error recv'd"
                    job.error_code = "usercmd_error"
                    job.timestamp = datetime.datetime.now()

                if msg["command"] == "output_error":
                    logger.critical("=====================================")
                    logger.critical("ERROR: output_error catched")
                    return_msg = "output_error recv'd"
                    job.error_code = "output_error"
                    job.timestamp = datetime.datetime.now()

                    ## sulsj: try to resubmit
                    self.checkIfAlive()

                if msg["command"] == "proc_mon":
                    job.proc_mon = msg["data"]
                    job.log_file = msg["data"]["log_file"]

                    ## keep track of mem and cpu
                    try:
                        #job.track_procmon_mem.append(float(job.proc_mon["memory"]))
                        job.track_procmon_mem.append(float(job.proc_mon["vms"]))
                        job.track_procmon_cpu.append(float(job.proc_mon["cpu_load"]))
                        job.track_procmon_runtime = int(job.proc_mon["runtime"])
                    except:
                        logger.warning("error decoding proc_mon data")

                    logger.info("job_name=%s, job_id=%s, mem=%s:%s, cpu=%s, runtime=%s, pidtree=%s, nodemem=%.1f, host=%s"
                                % (job.name,
                                   job.jobid,
                                   #job.proc_mon["memory"],
                                   job.proc_mon["vms"],
                                   job.proc_mon["rss"],
                                   job.proc_mon["cpu_load"],
                                   job.proc_mon["runtime"],
                                   job.proc_mon["pidtree"],
                                   job.proc_mon["free"],
                                   job.proc_mon["hostname"]))

                    return_msg = "proc_mon recv'd"
                    job.timestamp = datetime.datetime.now()

                if msg["command"] == "fetch_input":
                    return_msg = self.jobid_to_job[job_id]

                    ## sulsj: The job is in running state, thus record the
                    ## queue wait end time
                    job.qw_end.append(datetime.datetime.now())

                    logger.info( "Job '%s', '%s'  is running now." % (job_id, job.jobid))
                    logger.info( "Queue waiting time = %s" % (str(job.qw_end[-1] - job.qw_start[-1])))
               
                if msg["command"] == "store_output":
                    return_msg = "store_output recv'd"

                    ## store tmp job object
                    tmp_job = msg["data"]

                    ## copy relevant fields
                    job.ret = tmp_job.ret
                    job.exception = tmp_job.exception

                    ## is assigned in submission process and not written back
                    ## server-side
                    #job.log_stdout_fn = tmp_job.log_stdout_fn
                    job.timestamp = datetime.datetime.now()

                if msg["command"] == "heart_beat":
                    job.heart_beat = msg["data"]
                    job.log_file = msg["data"]["log_file"]

                    ## keep track of mem and cpu
                    try:
                        #job.track_mem.append(float(job.heart_beat["memory"]))
                        job.track_mem.append(float(job.heart_beat["vms"]))
                        job.track_cpu.append(float(job.heart_beat["cpu_load"]))
                        job.track_runtime = int(job.heart_beat["runtime"])
                    except:
                        print "error decoding heart-beat"

                    return_msg = "heart_beat recv'd"
                    job.timestamp = datetime.datetime.now()

                if msg["command"] == "get_job":
                    ## serve job for display
                    return_msg = job

                else:
                    ## update host name
                    job.host_name = msg["host_name"]

            ## if the job was qsub'd, keep checking
            else: ## job_id == -1:
                ## run check
                self.checkIfAlive()

                if msg["command"] == "get_jobs":
                    ## serve list of jobs for display
                    return_msg = self.jobs

            ## send back compressed response
            #print "[ZMQ in check] check() sends '%s'" % (str(return_msg))
            self.socket.send(zdumps(return_msg))

        #time.sleep(10) # for web page
        #if self.cherry_pid != -1:
        #    logger.debug("All jobs done, Kill the web server.")
        #    try:
        #        os.kill(self.cherry_pid, signal.SIGKILL)
        #    except Exception, e:
        #        print "Exception: failed to kill the web server."
        #        print e

        local_heart.terminate()


    def checkIfAlive(self):
        """
        check if jobs are alive and determine cause of death if not
        """

        ## sulsj
        #s = drmaa.Session()
        #s.initialize(self.session_id)

        for job in self.jobs:
            ## noting was returned yet
            if job.ret == None:
                ## sulsj
                ##
                #jobid = job.jobid
                #try:
                #    curstat = s.jobStatus(jobid)
                #except:
                #    curstat = -42
                #print "[DRMAA] status update for job", jobid, "to", curstat, "on", job.host_name

                ## exclude first-timers
                if job.timestamp != None:
                    # only check heart-beats if there was a long delay
                    current_time = datetime.datetime.now()
                    time_delta = current_time - job.timestamp

                    ## If there is not heartbeat for a while
                    if time_delta.seconds > CFG["MAX_TIME_BETWEEN_HEARTBEATS"]:
                        ## sulsj: check if output file(s) error
                        if job.error_code == "output_error":
                            logger.info("=====================================")
                            logger.info("job '%s, %s'  has no valid output file(s), '%s/%s'"  % (job.name, job.jobid, CFG["TEMPDIR"], job.output_file))
                            job.cause_of_death = "output_error"

                        ## sulsj: check if user command error
                        elif job.error_code == "usercmd_error":
                            print "Job '%s, %s'  has encountered an user command error"  % (job.name, job.jobid)
                            job.cause_of_death = "usercmd_error"
                        
                        ## sulsj: check out of wallclocktime
                        elif job.checkOutOfWallclocktime():
                            logger.info("=====================================")
                            logger.info("job '%s, %s' was out of wallclocktime" % (job.name, job.jobid))
                            job.cause_of_death = "out_of_wallclocktime"
                            
                        ## out-of-memory checking
                        elif job.checkOutOfMem():
                            logger.info("=====================================")
                            logger.info("job '%s, %s' was out of memory" % (job.name, job.jobid))
                            job.cause_of_death = "out_of_memory"

                        else:
                            ## To do: check if node is reachable
                            ## To do: check if network hangs, wait some more if so
                            logger.info("=====================================")
                            logger.info("job '%s, %s' died for unknown reason" % (job.name, job.jobid))
                            job.cause_of_death = "unknown"

            else: ## job.ret != None
                ## could have been an exception, we check right away
                if job.ret == "job dead" and job.error_code == "usercmd_error":
                    continue
                
                elif job.checkOutOfWallclocktime():
                    print "+Job hit the wallclocktime"
                    job.cause_of_death = "out_of_wallclocktime"
                    job.ret = None
                    
                elif job.checkOutOfMem():
                    print "+Job was out of memory"
                    job.cause_of_death = "out_of_memory"
                    job.ret = None

                elif isinstance(job.ret, Exception):
                    print "+Job encountered exception, will not resubmit"
                    job.cause_of_death = "exception"

                    ## Send an email report
                    if send_error_email(job) == 0:
                        logger.info("Email sent")
                    else:
                        logger.critical("Failed to send an error reporting email")
                    job.ret = "job dead (with non-memory related exception)"

            ## attempt to resubmit
            if job.cause_of_death == "out_of_memory" or job.cause_of_death == "out_of_wallclocktime" or job.cause_of_death == "output_error" or job.cause_of_death == "unknown" or job.cause_of_death == "usercmd_error":
                print "Creating error report and sending to ", CFG['EMAIL_TO']

                ## Send an email report
                if send_error_email(job) == 0:
                    logger.info("Email sent")
                else:
                    logger.critical("Failed to send an error reporting email")

                if job.cause_of_death == "usercmd_error":
                    print "Giving up the job because of user command error."
                    job.ret = "job dead"

                ## Try to resubmit
                elif not handleResubmit(self.session_id, job):
                    print "giving up on job"
                    job.ret = "job dead"

                ## break out of loop to avoid too long delay
                break

        ## sulsj
        #s.exit()

    def checkAllDone(self):
        """
        checks for all jobs if they are done
        """

        for job in self.jobs:
            # exceptions will be handled in checkIfAlive
            if job.ret == None or isinstance(job.ret, Exception):
                return False

        return True


#-------------------------------------------------------------------------------
def handleResubmit(session_id, job):
#-------------------------------------------------------------------------------
    """
    heuristic to determine if the job should be resubmitted

    side-effect:
    job.num_resubmits_* and num_total_resubmits incremented
    job.ram_c and job.h_vmem will be increased, if cause was 'out_of_memory'
    job.h_rt will be increased, if cause was 'out_of_wallclocktime'
    """

    ## reset some fields
    job.timestamp = None
    job.heart_beat = None

    #if job.num_resubmits < CFG["NUM_MAX_RESUBMITS"]:
    if job.num_resubmits_mem < CFG["NUM_MAX_RESUBMITS_MEM"] and job.num_resubmits_wallclock < CFG["NUM_MAX_RESUBMITS_WALLCLOCKTIME"] and job.num_resubmits_output < CFG["NUM_MAX_RESUBMITS_OUTPUT"]:

        #print "Looks like job died an unnatural death, resubmitting"
        print "previous out_of_mem resubmits = %i" % (job.num_resubmits_mem)
        print "previous out_of_wallclocktime resubmits = %i" % (job.num_resubmits_wallclock)
        print "previous output_error resubmits = %i" % (job.num_resubmits_output)

        if job.cause_of_death == "out_of_memory":
            ## increase memory
            job.num_resubmits_mem += 1
            job.increaseMemRequest(CFG["OUT_OF_MEM_INCREASE"])

        elif job.cause_of_death == "out_of_wallclocktime":
            ## increase h_rt
            job.num_resubmits_wallclock += 1
            job.increaseWalltimeRequest(CFG["OUT_OF_WALLCLOCKTIME_INCREASE"])

        elif job.cause_of_death == "output_error":
            ## resubmit the job again to get the outputs
            job.num_resubmits_output += 1

        else:
            ## remove node from white_list
            node_name = "all.q@" + job.host_name
            if job.white_list.count(node_name) > 0:
                job.white_list.remove(node_name)

        ## increment number of resubmits
        job.num_total_resubmits += 1

        ## Clear job status
        job.cause_of_death = ""
        job.error_code = ""

        resubmit(session_id, job)

        return True

    else:

        return False


#-------------------------------------------------------------------------------
def resubmit(session_id, job):
#-------------------------------------------------------------------------------
    """
    encapsulate creation of session for multiprocessing
    """

    print "starting resubmission process"

    ## append to session
    session = drmaa.Session()
    session.initialize(session_id)

    ## try to kill off old job
    try:
        ## To do: ask SGE more questions about job status etc (maybe
        ## re-integrate StatusChecker)
        ## To do: write unit test for this

        session.control(job.jobid, drmaa.JobControlAction.TERMINATE)
        print "zombie job killed"
    except Exception, detail:
        print "could not kill job with SGE id", job.jobid
        print detail

    ## To do: Do I have to clean all the stat data stored in the job object?
    #print debugmsg("left over: " + str(job.track_procmon_mem))
    #print debugmsg("left over: " + str(job.track_procmon_cpu))
    #print debugmsg("left over: " + str(job.track_procmon_runtime))

    ## create new job
    appendJobToSession(session, job)
    session.exit()



#####################################################################
# MapReduce Interface
#####################################################################


# def pg_map(f, args_list, param=None, local=False, maxNumThreads=1, mem="5G"):
#     """
#     provides a generic map function
#     """
#
#     jobs = []
#
#     # construct jobs
#     for args in args_list:
#         job = KybJob(f, [args], param=param)
#         job.h_vmem = mem
#
#         jobs.append(job)
#
#
#     # process jobs
#     processed_jobs = processJobs(jobs, local=local, maxNumThreads=maxNumThreads)
#
#     # store results
#     results = [job.ret for job in processed_jobs]
#
#     assert(len(jobs) == len(processed_jobs))
#     # make sure results are in the same order
#     # To do make reasonable check
#     #for idx in range(len(jobs)):
#     #    assert(jobs[idx].args == processed_jobs[idx].args)
#
#     return results


################################################################
#      Some handy functions
################################################################

#-------------------------------------------------------------------------------
def send_error_email(job):
#-------------------------------------------------------------------------------
    """
    send out diagnostic email
    """

    ## create message
    msg = ""
    subject= "JobLauncher error report for " + str(job.name)

    ##
    ## Set email recipient
    ##
    if CFG['EMAIL_TO'] != "":
        emailTo = CFG['EMAIL_TO']
    else:
        emailTo = getpass.getuser()

    ## compose error message
    body_text = ""

    body_text += "Job name, jobid: %s, %s" % (str(job.name), str(job.jobid)) + "\r\n"
    body_text += "User command: " + str(job.user_cmd) + "\r\n"
    body_text += "Output file(s): " + str(job.output_file) + "\r\n"
    body_text += "SGE params: ram.c=%s, h_vmem=%s, h_rt=%s, other_param=%s" % (job.ram_c, job.h_vmem, job.h_rt, job.sge_param) + "\r\n"
    body_text += "Last compute node name: " + str(job.host_name) + "\r\n"

    body_text += "Last timestamp: " + str(job.timestamp) + "\r\n"
    body_text += "Cause_of_death: " + str(job.cause_of_death) + "\r\n"

    body_text += "Total number of resubmits: " + str(job.num_total_resubmits) + "\r\n"
    body_text += "   Num_resubmits (qsub): " + str(job.num_resubmits_qsub) + "\r\n"
    body_text += "   Num_resubmits (mem): " + str(job.num_resubmits_mem) + "\r\n"
    body_text += "   Num_resubmits (wallclocktime): " + str(job.num_resubmits_wallclock) + "\r\n"
    body_text += "   Num_resubmits (output): " + str(job.num_resubmits_output) + "\r\n"

    body_text += "Queue wait time: \r\n"
    for i in range(min(len(job.qw_start), len(job.qw_end))):
        body_text += str(job.qw_end[i] - job.qw_start[i]) + "\r\n"

    if job.proc_mon:
        body_text += "User job resource consumption ---\r\n"
        #body_text += "   memory usage: " + str(job.proc_mon["memory"]) + "\r\n"
        body_text += "   memory usage: " + str(job.proc_mon["vms"]) + "\r\n"
        body_text += "   cpu load: " + str(job.proc_mon["cpu_load"]) + "\r\n"
        body_text += "   runtime: " + str(job.proc_mon["runtime"]) + " seconds\r\n"

    if isinstance(job.ret, Exception):
        body_text += "job encountered exception: " + str(job.ret) + "\r\n"
        body_text += "stacktrace: " + str(job.exception) + "\r\n\r\n"

    print body_text

    msg += body_text + '\r\n'

    ## attach log file
    if job.heart_beat:
        log_file = open(job.heart_beat["log_file"], "r")
        for l in log_file.readline():
            msg += l
        log_file.close()

    ##
    ## Send a report by email
    ##
    return sendEmail(emailTo, subject, msg, CFG['EMAIL_FROM'], logger)



################################################################
#      The following code will be executed on the cluster      #
################################################################

#-------------------------------------------------------------------------------
def heart_beat(job_id, address, cmd, who="", parent_pid=-1, log_file="", wait_sec=45):
#-------------------------------------------------------------------------------
    """
    will send reponses to the server with
    information about the current state of
    the process
    """
    
    ##
    ## To do:
    ## In the begining stage, we need to send HB very frequently to catch
    ## abrupt memory usage increase which could result in out-of-memory status.
    ## Thus ignore wait_sec for about 10-20 sec and just send the HB in every
    ## 0.5~1 sec.
    ##
    while True:
        #status = get_job_status(parent_pid)
        status = get_job_status2(parent_pid)
        status["log_file"] = log_file
        send_zmq_msg(job_id, cmd, status, address, who)
        
        time.sleep(wait_sec)


##-------------------------------------------------------------------------------
#def get_job_status(parent_pid):
##-------------------------------------------------------------------------------
#    """
#    script to determine the status of the current
#    worker and its machine (currently not cross-platform)
#    """
#
#    status_container = {}
#
#    if parent_pid != -1:
#        status_container["memory"] = get_memory_usage(parent_pid)
#        status_container["cpu_load"] = get_cpu_load(parent_pid)
#        status_container["runtime"] = get_runtime(parent_pid)
#
#    return status_container


#-------------------------------------------------------------------------------
def get_job_status2(parent_pid):
#-------------------------------------------------------------------------------
    """
    script to determine the status of the current
    worker and its machine (currently not cross-platform)
    """

    status_container = {}

    if parent_pid != -1:
        ## Collect pids from process tree
        pidListMerged = []
        pidListRoot = get_pid_tree(parent_pid)
        pidListMerged = pidListRoot
        #logger.debug("pids: %s" % (pidListMerged))
        print "pids: %s" % (pidListMerged)
                
        vmemUsageList = []
        vmemUsageList.extend([get_virtual_memory_usage(pid, 0.0, False) for pid in pidListMerged])
        print "VMS: %s" % (vmemUsageList)
        
        rmemUsageList = []
        rmemUsageList.extend([get_resident_memory_usage(pid, 0.0, False) for pid in pidListMerged])
        print "RSS: %s" % (rmemUsageList)
        
        ## Collect cpu_usages for all pids in the tree and get max()
        cpuLoadList = [get_cpu_load(pid) for pid in pidListMerged]
        cpuLoad = max(cpuLoadList)
        
        ## Collect mem_usages for all pids in the tree and get sum()
        rmemUsage = sum(rmemUsageList)
        vmemUsage = sum(vmemUsageList)
        
        ## Get total memory usage on this node
        nodeMemUsage = get_total_mem_usage_per_node()
        
        #status_container["memory"] = vmemUsage
        status_container["vms"] = vmemUsage
        status_container["rss"] = rmemUsage
        status_container["cpu_load"] = cpuLoad
        status_container["runtime"] = get_runtime(parent_pid)
        status_container["pidtree"] = str(pidListMerged)
        status_container["free"] = nodeMemUsage
        status_container["hostname"] = socket.gethostname()

    return status_container

#-------------------------------------------------------------------------------
def checkOutput(job_id, job, address):
#-------------------------------------------------------------------------------
    """
    Check outout files in out_dir

    """

    ## If there is no output file to check is specified, return OK
    if len(job.output_file) == 0:
        return True

    outfiles = job.output_file
    f_exist = False
    f_size = 0
    ok = False
    trial = 0
    max_trials = CFG['NUM_MAX_OUTPUT_CHECK']
    wait_sec_orig = CFG['OUTPUT_CHECK_SLEEP_TIME']
    wait_sec_inc = CFG['OUTPUT_CHECK_SLEEP_INCREASE']
    bAllowZeroSizeOutput = CFG['ALLOW_ZERO_SIZE_OUTPUT']
    
    for ofileTmp in outfiles:
        ofile = ofileTmp
        print ("Output file check: %s", ofile)

        f_exist = False
        f_size = 0
        ok = False

        while trial < max_trials:
            print ("Output file checking. Trial# = %d" % (trial))

            time.sleep(wait_sec_orig)

            ## First, check file existence
            f_exist = os.path.exists(ofile)

            ## If exist, check file size
            if f_exist and os.path.isfile(ofile):
                f_size = os.path.getsize(ofile)
                if bAllowZeroSizeOutput and f_size == 0: f_size = 1
                if f_size == 0:
                    print ('File, %s is zero size.' % (ofile))

            if f_exist and f_size > 0:
                ok = True
                print ("=====================================")
                print ("Output file '%s' is OK." % (ofile))
                break

            wait_sec_orig *= wait_sec_inc
            trial += 1

    return ok


#-------------------------------------------------------------------------------
def run_job(job_id, address):
#-------------------------------------------------------------------------------
    """
    This is the code that is executed on the cluster side.

    @param job_id: unique id of job
    @type job_id: string
    """

    wait_sec = random.randint(0, 5)
    time.sleep(wait_sec)

    try:
        job = send_zmq_msg(job_id, "fetch_input", None, address,
                           "compute node: run_job()")
    except Exception, e:
        ## here we will catch errors caused by pickled objects
        ## of classes defined in modules not in PYTHONPATH
        print e

        ## send back exception
        thank_you_note = send_zmq_msg(job_id, "store_output", e, address,
                                      "compute node: run_job() --> exception caused by pickled objects")
        print thank_you_note

        return

    parent_pid = os.getpid()

    ## create heart beat process
    heart = multiprocessing.Process(target=heart_beat, args=(job_id, address,
                                                             "heart_beat",
                                                             "run_job() in compute node",
                                                             parent_pid,
                                                             job.log_stdout_fn,
                                                             CFG["HEARTBEAT_FREQUENCY"]))
    heart.start()

    ## change working directory
    if 1:
        if job.working_dir is not None:
            os.chdir(job.working_dir)

    # run job
    job.execute()

    ## sulsj: something's wrong with the user command in job.user_cmd
    if job.ret != 0:
        ## send back abnormal exit of the user command
        print ("Error: user_cmd_error, '%s'" % (str(job.ret)))
        usercmd_error_note = send_zmq_msg(job_id, "usercmd_error", job, address,
                                          "compute node: run_job()")
        logger.debug(usercmd_error_note)

    else:
        ##########################
        # Output file checking
        ##########################
        print ( "Output file checking for: '%s'" % (job.output_file))
        # if not checkOutput(job_id, job, address, out_dir):
        if not checkOutput(job_id, job, address):
            ## send back output_error
            print ("=====================================")
            print ("Error: output_error, %s" % (str(job.output_file)))
            ouput_error_note = send_zmq_msg(job_id, "output_error", job, address,
                                            "compute node: run_job()")
            logger.debug(ouput_error_note)

        else:
            ## send back result
            print ("Output file(s) are checked and all ok!:  %s"
                   % (str(job.output_file)))
            thank_you_note = send_zmq_msg(job_id, "store_output", job, address,
                                          "compute node: run_job()")
            logger.debug(thank_you_note)

    # stop heartbeat
    heart.terminate()


#-------------------------------------------------------------------------------
def send_zmq_msg(job_id, command, data, address, who):
#-------------------------------------------------------------------------------
    """
    simple code to send messages back to host
    (and get a reply back)
    """

    context = zmq.Context()
    zsocket = context.socket(zmq.REQ)
    zsocket.connect(address)
    host_name = socket.gethostname()
    try:
        ip_address = socket.gethostbyname(host_name)
    except Exception, e:
        print e
        ip_address = "unknown host"
        print ("Exception: unknown host from socket.gethostbyname(host_name)")

    msg_container = {}
    msg_container["job_id"] = job_id
    msg_container["host_name"] = host_name
    msg_container["ip_address"] = ip_address
    msg_container["command"] = command
    msg_container["data"] = data

    msg_string = zdumps(msg_container)

    #logger.debug("\n[ZMQ] sent msg = '" + str(msg_container) + "' by " + str(who))
    zsocket.send(msg_string)
    msg = zloads(zsocket.recv())
    #logger.debug("[ZMQ] recv'd msg = '" + str(msg) + "' by " + str(who))

    return msg


#-------------------------------------------------------------------------------
class Usage(Exception):
#-------------------------------------------------------------------------------
    """
    Simple Exception for cmd-line user-interface.
    """

    def __init__(self, msg):
        """
        Constructor of simple Exception.

        @param msg: exception message
        @type msg: string
        """

        self.msg = msg


#-------------------------------------------------------------------------------
def main(argv=None):
#-------------------------------------------------------------------------------
    """
    Parse the command line inputs and call run_job

    @param argv: list of arguments
    @type argv: list of strings
    """

    if argv is None:
        argv = sys.argv

    try:
        try:
            opts, args = getopt.getopt(argv[1:], "h", ["help"])
            ## sulsj
            ## These should be set for run_something()
            ## To do: do I have to do like this?
            CFG['JOBID'] = args[0]
            CFG['HOMEADDR'] = args[1]

            run_job(args[0], args[1])

        except getopt.error, msg:
            raise Usage(msg)


    except Usage, err:
        print >>sys.stderr, err.msg
        print >>sys.stderr, "for help use --help"

        return 2


if __name__ == "__main__":
    sys.exit(main(sys.argv))


# EOF
