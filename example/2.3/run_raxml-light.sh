#!/bin/sh

module load python
module load openmpi
module load RAxML
module load RAxML-Light
module load ExaML

RAXML=/usr/common/jgi/phylogenetics/RAxML/git/bin/raxmlHPC-SSE3
RAXML_LIGHT_MPI=/usr/common/jgi/phylogenetics/RAxML-Light/1.0.9/bin/raxmlLight-MPI
RAXML_LIGHT_PTHREADS=/usr/common/jgi/phylogenetics/RAxML-Light/1.0.9/bin/raxmlLight-PTHREADS
RAXML_MODEL=PROTGAMMALG
rootDir=`pwd`
JOBLAUNCHER=/global/scratch/sd/sulsj/joblauncher-test/2.3/joblauncher.py
 
NPHYLA=100
NBASES=100
NTHREADS=8
SEED=9999

workingDir=`pwd`/out-raxml/${NPHYLA}_$NBASES
alignFile=`pwd`/data/${NPHYLA}_$NBASES.phyl
raxmlName=${NPHYLA}_$NBASES.start.t
echo $alignFile
echo $raxmlName

rm $workingDir/"RAxML_info."$raxmlName
rm $workingDir/"RAxML_parsimony."$raxmlName

mkdir -p $workingDir
cd $workingDir

## first get starting tree
$RAXML -y -n $raxmlName -m $RAXML_MODEL -s $alignFile -p $SEED
retcode=$?

if [ $retcode -ne 0 ]; then
    echo "Couldn't generate starting tree, dumping out."
    exit $retcode
else
	echo "Successful to generate a starting tree, $raxmlName."
fi

cd $rootDir

# run raxml-light
#cmd="aprun -n $NCORES -S $NNUMA -sn $NCPNUMA $RAXML_LIGHT_MPI -m $RAXML_MODEL -s $alignFile -D -t RAxML_parsimonyTree.$raxmlName -n light_$raxmlName"

OUTPUT=light_$raxmlName
# cmd="$RAXML_LIGHT_PTHREADS -T $NTHREADS -m $RAXML_MODEL -s $alignFile -D -t RAxML_parsimonyTree.$raxmlName -n $OUTPUT"
cmd="$RAXML_LIGHT_PTHREADS -T $NTHREADS -m $RAXML_MODEL -s $alignFile -D -t $workingDir/RAxML_parsimonyTree.$raxmlName -n $OUTPUT"

OUTPUT2=RAxML_result.light_$raxmlName
# jobcmd="$JOBLAUNCHER --name p${NPHYLA}p_${NBASES}s --ram_c 8G --h_vmem 8G --h_rt 00:30:00 --sge_param ' -j y -shell y -S /bin/bash -w e -cwd -pe pe_1 1   ' --od $workingDir  --cmd '$cmd' --retry 3 --delay 30 --cf $OUTPUT2"
jobcmd="$JOBLAUNCHER --name p${NPHYLA}p_${NBASES}s_test --ramc 4G --hvmem 4G --hrt 00:30:00 --sgeparam ' -j y -shell y -S /bin/bash -w e -cwd -pe pe_1 1 -l high.c ' --cmd '$cmd' --retry 3 --delay 30 --checkfile $rootDir/$OUTPUT2 "

# cd $rootDir
echo $cmd
echo $jobcmd
eval $jobcmd

#echo `cat p* | grep elapsed`
echo `cat *info* | grep accumulated`

## EOF
