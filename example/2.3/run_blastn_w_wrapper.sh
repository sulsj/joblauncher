#!/bin/sh

module load python
JOBLAUNCHER=`pwd`/joblauncher.py
# $JOBLAUNCHER --name blastall_test1 --ramc 5G --hvmem 5G --hrt 00:01:00 --sgeparam ' -j y -shell y -S /bin/bash -w e -cwd -l high.c -pe pe_1 1 ' --checkfile ./out-blastn/test2.m8.bout,./out-blastn/test2.m8.bout --cmd '/jgi/tools/bin/blastall -b 100 -v 100 -K 100 -p blastn -S 3 -d ./data/hs.m51.D4.diplotigs+fullDepthIsotigs.fa -e 10 -F F -W 41 -i ./data/nucl_input.fna -m 8 -o ./out-blastn/test2.m8.bout' --retry 3 --delay 10 --loglevel debug 
$JOBLAUNCHER --name blastall_wrapper_test1 --ramc 5G --hvmem 5G --hrt 00:30:00 --sgeparam ' -j y -shell y -S /bin/bash -w e -cwd -l high.c -pe pe_1 1 ' --checkfile ./out-blastn/test1.m8.bout --cmd './blastallrun.sh ' --retry 3 --delay 10 --loglevel debug 
