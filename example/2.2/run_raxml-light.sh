#!/bin/sh

module load python
module load openmpi
module load RAxML
module load RAxML-Light
module load ExaML

RAXML=/usr/common/jgi/phylogenetics/RAxML/git/bin/raxmlHPC-SSE3
RAXML_LIGHT_MPI=/usr/common/jgi/phylogenetics/RAxML-Light/1.0.9/bin/raxmlLight-MPI
RAXML_LIGHT_PTHREADS=/usr/common/jgi/phylogenetics/RAxML-Light/1.0.9/bin/raxmlLight-PTHREADS
RAXML_MODEL=PROTGAMMALG
JOBLAUNCHER=`pwd`/joblauncher.py
 
NPHYLA=100
NBASES=100
NTHREADS=8
SEED=9999

workingDir=`pwd`/out-raxml/${NPHYLA}_$NBASES
alignFile=`pwd`/data/${NPHYLA}_$NBASES.phyl
raxmlName=${NPHYLA}_$NBASES.start.t
echo $alignFile
echo $raxmlName

rm $workingDir/"RAxML_info."$raxmlName


mkdir -p $workingDir
cd $workingDir

## first get starting tree
$RAXML -y -n $raxmlName -m $RAXML_MODEL -s $alignFile -p $SEED
retcode=$?

if [ $retcode -ne 0 ]; then
    echo "Couldn't generate starting tree, dumping out."
    exit $retcode
fi

## run raxml-light
#cmd="aprun -n $NCORES -S $NNUMA -sn $NCPNUMA $RAXML_LIGHT_MPI -m $RAXML_MODEL -s $alignFile -D -t RAxML_parsimonyTree.$raxmlName -n light_$raxmlName"

OUTPUT=light_$raxmlName
cmd="$RAXML_LIGHT_PTHREADS -T $NTHREADS -m $RAXML_MODEL -s $alignFile -D -t RAxML_parsimonyTree.$raxmlName -n $OUTPUT"

OUTPUT2=RAxML_result.$OUTPUT
jobcmd="$JOBLAUNCHER --name p${NPHYLA}p_${NBASES}s --ram_c 2G --h_vmem 2G --h_rt 02:00:00 --sge_param '-j y -shell y -S /bin/bash -w e -cwd -l debug.c -pe pe_8 8 -R y ' --od $workingDir  --cmd '$cmd' --retry 3 --delay 30 --cf $OUTPUT2"

echo $cmd
echo $jobcmd
eval $jobcmd

#echo `cat p* | grep elapsed`
echo `cat *info* | grep accumulated`