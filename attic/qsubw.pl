#!/jgi/tools/bin/perl

=head1 NAME

qsubw.pl

=head1 SYNOPSIS

  qsubw.pl [options] <cmd>

  Options:
  -sge <cluster>   Specify sge cluster (required)
  -sgeBin <dir>    Specify sge bin dir (optional)
  -sgeRoot <dir>   Specify sge root dir (optional)
  -p <string>      Qsub params (optional).
  -od <dir>        Specify the output data dir (optional; default=current dir).
  -cf <file>       Check for this output file (optional; use multiple flags
                   to check for more files).
  -retry <number>  Resubmit job on cluster this many times if no job id was
                   assigned, or if using the -cf option, a file was not found
                   (optional).
  -delay <seconds> When using the -retry option, wait this many seconds before
                   resubmitting the job (optional; default=300 secs.)
  -h               Detailed message (optional).
  
=head1 VERSION

$Revision$

$Date$

=head1 AUTHOR(S)

Stephan Trong

=head1 HISTORY

=over

=item *

S.Trong 2012/01/17 creation

=back

=cut

use strict;
use warnings;
use Pod::Usage;
use Getopt::Long;
use FileHandle;
use File::Copy;
use File::Path;
use File::Find;
use File::Basename;
use Cwd;
use Cwd qw (realpath getcwd abs_path);
use FindBin qw($RealBin);
use lib "$RealBin/../lib";
use PGF::Jigsaw::Sge;
use vars qw( $optHelp $optOutputDir $optSgeParams $optSgeBinDir $optSgeRoot
    $optSgeCluster @optCheckFiles $optRetry $optDelayTime);

#============================================================================#
# CHECKS
#============================================================================#
if( !GetOptions(
                "od=s"=>\$optOutputDir,
                "p=s"=>\$optSgeParams,
                "sgeBin=s"=>\$optSgeBinDir,
                "sgeRoot=s"=>\$optSgeRoot,
                "sge=s"=>\$optSgeCluster,
                "cf=s@"=>\@optCheckFiles,
                "retry=n"=>\$optRetry,
                "delay=n"=>\$optDelayTime,
                "h" =>\$optHelp,
               )
    ) {
    printhelp(1);
}

printhelp(2) if $optHelp;
printhelp(1) if @ARGV < 1;

#============================================================================#
# INITIALIZE VARIABLES
#============================================================================#

use constant SUCCESS => 0;
use constant FAILURE => 1;

my $outputDir = $optOutputDir ? $optOutputDir : getcwd;
   $outputDir = abs_path($outputDir);
   $outputDir =~ s/\/$//;
my $OBJ_SGE = PGF::Jigsaw::Sge->new($optSgeCluster, $optSgeBinDir,
    $optSgeRoot);
my $JOB_ID = 0;
   
my $cmd = shift @ARGV;

$optRetry = 0 if !defined $optRetry;
$optDelayTime = 300 if !defined $optDelayTime;

#============================================================================#
# MAIN
#============================================================================#

# Set file permission so that you and group can read, write, execute.
#
umask 0002;

# Capture signal handlers and quit nicely.
#
$SIG{'INT'} = \&cancelExecution;
$SIG{'TERM'} = \&cancelExecution;
$SIG{'KILL'} = \&cancelExecution;

# Create output dir if not exist.
#
mkpath( $outputDir ) if ($optOutputDir && !-e $outputDir);

my $numRetries = 0;
my $success = 0;
while ( !$success && $numRetries <= $optRetry ) {
    $numRetries++;
    my $exitStatus = runCommandSGE($cmd, $optSgeParams);
    
    # If command submitted to sge successfully and ran to completion, check for
    # output files if -cf flag(s) are used.
    #
    if ( $exitStatus == SUCCESS ) {
        if ( @optCheckFiles ) {
            if ( checkOutputFiles(@optCheckFiles) == SUCCESS) {
                $success = 1;
                last;
            } else {
                $exitStatus = FAILURE;
            }
        } else {
            $success = 1;
            last;
        }
    } else {
        $numRetries -= 0.9;
        print STDERR "Will resubmit in $optDelayTime seconds ...\n\n";
        sleep $optDelayTime;
    }

    # If command failed, print message to resubmit command.
    #
    if ( $exitStatus != SUCCESS && $numRetries <= $optRetry ) {
        print STDERR "DATE: ".getCurrentDateAndTime()."\n";
        print STDERR "Resubmitting $cmd ... $numRetries of $optRetry retries.\n\n";
    }
}

exit 0;

#============================================================================#
# SUBROUTINES
#============================================================================#
sub runCommandSGE {
    my $cmd = shift;
    my $sgeParams = shift || '';
    
    my $jobName = $cmd;
       $jobName =~ s/^(\S+)\s+.*/$1/;
    $jobName = basename($jobName);
    
    if ( $sgeParams =~ /\-N +(\S+)/ ) {
        $jobName = $1;
    } else {
        $sgeParams = "-N $jobName $sgeParams";
    }
    
    my ($jobId, $msg) = $OBJ_SGE->submit($sgeParams, $cmd, 1);
    
    if ( !defined $jobId || !$jobId ) {
        $msg = "No job id assigned on $optSgeCluster." if (!defined $msg ||
            !length $msg);
        print STDERR "Error submitting job to sge cluster: $msg\n";
        return FAILURE;
    }
    
    print "$msg on $optSgeCluster\n\n";

    $JOB_ID = $jobId;
    
    # Wait for sge job to complete
    #
    $OBJ_SGE->wait($jobId);
    
    return SUCCESS;
}

#============================================================================#
sub checkOutputFiles {
    my @files = @_;
    
    my $retry = 0;
    my $maxRetry = 20;
    my $sleepTime = 30; # retry 20 x every 30 secs for a total of 10 min.
    my $fileNotFound = 0;
    my $file = '';
    foreach $file (@files) {
        if ( -d $file || !-s $file ) {
            # Due to lag in file system network, will retry checking for
            # file with a max of $maxRetry times, sleeping every $sleepTime
            # secs. If found, proceed to next file. Otherwise, return failure.
            #
            while ($retry < $maxRetry) {
                $retry++;
                sleep $sleepTime;
                last if -s $file;
            }
            if ( !-s $file && $retry == $maxRetry ) {
                print STDERR "DATE: ".getCurrentDateAndTime()."\n";
                print STDERR "File $file does not exist or is zero size ".
                "(checked $retry times every $sleepTime with a timeout of ".
                ($retry*$sleepTime)." secs).\n\n";
                return FAILURE;
            }
        }
        $fileNotFound = 1 if !-s $file;
    }

    if ( $fileNotFound ) {
        print STDERR "DATE: ".getCurrentDateAndTime()."\n";
        print STDERR "File $file does not exist or is zero size ".
        "(checked $retry times every $sleepTime with a timeout of ".
        ($retry*$sleepTime)." secs).\n\n";
        return FAILURE;
    }

    return SUCCESS;
}

#============================================================================#
sub cancelExecution {
    print STDERR "Command cancelled upon request.\n";
    $OBJ_SGE->cancel($JOB_ID) if $JOB_ID;
    exit 1;
}

#============================================================================#
sub printhelp {
    my $verbose = shift || 1;
    pod2usage(-verbose=>$verbose);
    exit 1;
}

#============================================================================#
sub getCurrentDateAndTime {

# Returns current date and time in format:
# 'MM-DD-YYYY HH24:MI:SS'

    my ($sec,$min,$hour,$day,$mon,$year) = localtime(time);

    $mon++;
    $year+=1900;

    my $time = sprintf( "%02d/%02d/%04d %02d:%02d:%02d", 
        $mon,$day,$year,$hour,$min,$sec );

    return $time;

}
    
#============================================================================#
