""" Example task list """
"""
Field Description:

    taskList =
        [
            {
            "name":"<job_name or "" if need to set a random name>", 
            "ram_c":"<ram.c or "" if need to set the same with h_vmem>",
            "h_vmem":"<h_vmem>",
            "h_rt":"<wallclocktime>",
            "sge_param":"<other sge params.",
            "output_file":["<list of output file names>"],
            "cmd":"<user command to run>"
            }
        ]
"""

taskList = [
    {"name":"test1",
     "ram_c":"1G",
     "h_vmem":"1G",
     "h_rt":"00:05:00",
     "sge_param":" -j y -shell y -S /bin/bash -w e -cwd -l debug.c -pe pe_slots 1 ",
     "output_file":["/global/scratch/sd/sulsj/pythongrid-temp-dir/test1.m8.bout", "/global/scratch/sd/sulsj/pythongrid-temp-dir/SUCCESS"],
     "cmd":"/jgi/tools/bin/blastall -b 100 -v 100 -K 100 -p blastn -S 3 -d /global/scratch/sd/sulsj/eugene/BLASTDB/hs.m51.D4.diplotigs+fullDepthIsotigs.fa -e 1e-10 -F F -W 41 -i /project/projectdirs/genomes/sulsj/test/2012.07.24-pythongrid-test/data/blast_query_1_160.fna -m 8 -o /global/scratch/sd/sulsj/pythongrid-temp-dir/test1.m8.bout"}
    ,
    {"name":"test2",
     "ram_c":"2G",
     "h_vmem":"2G",
     "h_rt":"00:02:00",
     "sge_param":" -j y -shell y -S /bin/bash -w e -cwd -l debug.c -pe pe_slots 1 ",
     "output_file":["/global/scratch/sd/sulsj/pythongrid-temp-dir/test2.m8.bout", "/global/scratch/sd/sulsj/pythongrid-temp-dir/SUCCESS"],
     "cmd":"/jgi/tools/bin/blastall -b 100 -v 100 -K 100 -p blastn -S 3 -d /global/scratch/sd/sulsj/eugene/BLASTDB/hs.m51.D4.diplotigs+fullDepthIsotigs.fa -e 1e-10 -F F -W 41 -i /project/projectdirs/genomes/sulsj/test/2012.07.24-pythongrid-test/data/blast_query_1_80.fna -m 8 -o /global/scratch/sd/sulsj/pythongrid-temp-dir/test2.m8.bout"}
    ,
     {"name":"test3",
     "ram_c":"2G",
     "h_vmem":"2G",
     "h_rt":"00:10:00",
     "sge_param":" -j y -shell y -S /bin/bash -w e -cwd -l debug.c -pe pe_slots 1 ",
     "output_file":["/global/scratch/sd/sulsj/pythongrid-temp-dir/test3.m8.bout", "/global/scratch/sd/sulsj/pythongrid-temp-dir/SUCCESS"],
      "cmd":"/jgi/tools/bin/blastall -b 100 -v 100 -K 100 -p blastn -S 3 -d /global/scratch/sd/sulsj/eugene/BLASTDB/hs.m51.D4.diplotigs+fullDepthIsotigs.fa -e 1e-10 -F F -W 41 -i /project/projectdirs/genomes/sulsj/test/2012.07.24-pythongrid-test/data/long.fna -m 8 -o /global/scratch/sd/sulsj/pythongrid-temp-dir/test3.m8.bout"}
     ,
     #{"name":"test4",
     #"ram_c":"2G",
     #"h_vmem":"2G",
     #"h_rt":"00:05:00",
     #"sge_param":" -j y -shell y -S /bin/bash -w e -cwd -l debug.c -pe pe_slots 1 ",
     #"output_file":["/global/scratch/sd/sulsj/pythongrid-temp-dir/test4.m8.bout", "/global/scratch/sd/sulsj/pythongrid-temp-dir/SUCCESS"],
     # "cmd":"/jgi/tools/bin/blastall -b 100 -v 100 -K 100 -p blastn -S 3 -d /global/scratch/sd/sulsj/eugene/BLASTDB/hs.m51.D4.diplotigs+fullDepthIsotigs.fa -e 1e-10 -F F -W 41 -i /project/projectdirs/genomes/sulsj/test/2012.07.24-pythongrid-test/data/blast_query_1_999.fna -m 8 -o /global/scratch/sd/sulsj/pythongrid-temp-dir/test4.m8.bout"}
     #,
     {"name":"",
     "h_vmem":"2G",
     "h_rt":"00:05:00",
     "sge_param":" -j y -shell y -S /bin/bash -w e -cwd -l debug.c -pe pe_slots 1 ",
     "output_file":["/global/scratch/sd/sulsj/pythongrid-temp-dir/test5.m8.bout", "/global/scratch/sd/sulsj/pythongrid-temp-dir/SUCCESS"],
      "cmd":"/jgi/tools/bin/blastall -b 100 -v 100 -K 100 -p blastn -S 3 -d /global/scratch/sd/sulsj/eugene/BLASTDB/hs.m51.D4.diplotigs+fullDepthIsotigs.fa -e 1e-10 -F F -W 41 -i /project/projectdirs/genomes/sulsj/test/2012.07.24-pythongrid-test/data/blast_query_1_160.fna -m 8 -o /global/scratch/sd/sulsj/pythongrid-temp-dir/test5.m8.bout"}
    # ,
    #{"name":"usearch_test1",
    # "ram_c":"2G",
    # "h_vmem":"2G",
    # "h_rt":"00:10:00",
    # "sge_param":" -j y -shell y -S /bin/bash -w e -cwd -l debug.c -pe pe_slots 1 ",
    # "output_file":["/global/scratch/sd/sulsj/pythongrid-temp-dir/results.blast", "/global/scratch/sd/sulsj/pythongrid-temp-dir/usearch.log"],
    #  "cmd":"/project/projectdirs/genomes/sulsj/test/usearch-test/usearch32 --query /project/projectdirs/genomes/sulsj/test/2012.07.24-pythongrid-test/data/short.fna --db /global/scratch/sd/sulsj/pythongrid-temp-dir/nucl_input.fna --blastout /global/scratch/sd/sulsj/pythongrid-temp-dir/results.blast --blast6out /global/scratch/sd/sulsj/pythongrid-temp-dir/results.b6 -evalue 0.01  --log /global/scratch/sd/sulsj/pythongrid-temp-dir/usearch.log"}
    #,
    #    {"name":"",
    # "ram_c":"1G",
    # "h_vmem":"1G",
    # "h_rt":"00:05:00",
    # "sge_param":" -j y -shell y -S /bin/bash -w e -cwd -l debug.c -pe pe_slots 1 ",
    # "output_file":["/global/scratch/sd/sulsj/pythongrid-temp-dir/test1.m8.bout", "/global/scratch/sd/sulsj/pythongrid-temp-dir/SUCCESS"],
    # "cmd":"/jgi/tools/bin/blastall -b 100 -v 100 -K 100 -p blastn -S 3 -d /global/scratch/sd/sulsj/eugene/BLASTDB/hs.m51.D4.diplotigs+fullDepthIsotigs.fa -e 1e-10 -F F -W 41 -i /project/projectdirs/genomes/sulsj/test/2012.07.24-pythongrid-test/data/blast_query_1_160.fna -m 8 -o /global/scratch/sd/sulsj/pythongrid-temp-dir/test1.m8.bout"}
    #,
    #{"name":"",
    # "ram_c":"2G",
    # "h_vmem":"2G",
    # "h_rt":"00:02:00",
    # "sge_param":" -j y -shell y -S /bin/bash -w e -cwd -l debug.c -pe pe_slots 1 ",
    # "output_file":["/global/scratch/sd/sulsj/pythongrid-temp-dir/test2.m8.bout", "/global/scratch/sd/sulsj/pythongrid-temp-dir/SUCCESS"],
    # "cmd":"/jgi/tools/bin/blastall -b 100 -v 100 -K 100 -p blastn -S 3 -d /global/scratch/sd/sulsj/eugene/BLASTDB/hs.m51.D4.diplotigs+fullDepthIsotigs.fa -e 1e-10 -F F -W 41 -i /project/projectdirs/genomes/sulsj/test/2012.07.24-pythongrid-test/data/blast_query_1_80.fna -m 8 -o /global/scratch/sd/sulsj/pythongrid-temp-dir/test2.m8.bout"}
    #,
    # {"name":"",
    # "ram_c":"2G",
    # "h_vmem":"2G",
    # "h_rt":"00:10:00",
    # "sge_param":" -j y -shell y -S /bin/bash -w e -cwd -l debug.c -pe pe_slots 1 ",
    # "output_file":["/global/scratch/sd/sulsj/pythongrid-temp-dir/test3.m8.bout", "/global/scratch/sd/sulsj/pythongrid-temp-dir/SUCCESS"],
    #  "cmd":"/jgi/tools/bin/blastall -b 100 -v 100 -K 100 -p blastn -S 3 -d /global/scratch/sd/sulsj/eugene/BLASTDB/hs.m51.D4.diplotigs+fullDepthIsotigs.fa -e 1e-10 -F F -W 41 -i /project/projectdirs/genomes/sulsj/test/2012.07.24-pythongrid-test/data/long.fna -m 8 -o /global/scratch/sd/sulsj/pythongrid-temp-dir/test3.m8.bout"}
    # ,
    # {"name":"",
    # "ram_c":"2G",
    # "h_vmem":"2G",
    # "h_rt":"00:05:00",
    # "sge_param":" -j y -shell y -S /bin/bash -w e -cwd -l debug.c -pe pe_slots 1 ",
    # "output_file":["/global/scratch/sd/sulsj/pythongrid-temp-dir/test4.m8.bout", "/global/scratch/sd/sulsj/pythongrid-temp-dir/SUCCESS"],
    #  "cmd":"/jgi/tools/bin/blastall -b 100 -v 100 -K 100 -p blastn -S 3 -d /global/scratch/sd/sulsj/eugene/BLASTDB/hs.m51.D4.diplotigs+fullDepthIsotigs.fa -e 1e-10 -F F -W 41 -i /project/projectdirs/genomes/sulsj/test/2012.07.24-pythongrid-test/data/blast_query_1_999.fna -m 8 -o /global/scratch/sd/sulsj/pythongrid-temp-dir/test4.m8.bout"}
    # ,
    # {"name":"",
    # "h_vmem":"2G",
    # "h_rt":"00:05:00",
    # "sge_param":" -j y -shell y -S /bin/bash -w e -cwd -l debug.c -pe pe_slots 1 ",
    # "output_file":["/global/scratch/sd/sulsj/pythongrid-temp-dir/test5.m8.bout", "/global/scratch/sd/sulsj/pythongrid-temp-dir/SUCCESS"],
    #  "cmd":"/jgi/tools/bin/blastall -b 100 -v 100 -K 100 -p blastn -S 3 -d /global/scratch/sd/sulsj/eugene/BLASTDB/hs.m51.D4.diplotigs+fullDepthIsotigs.fa -e 1e-10 -F F -W 41 -i /project/projectdirs/genomes/sulsj/test/2012.07.24-pythongrid-test/data/blast_query_1_160.fna -m 8 -o /global/scratch/sd/sulsj/pythongrid-temp-dir/test5.m8.bout"}
    # ,
    #{"name":"",
    # "ram_c":"2G",
    # "h_vmem":"2G",
    # "h_rt":"00:10:00",
    # "sge_param":" -j y -shell y -S /bin/bash -w e -cwd -l debug.c -pe pe_slots 1 ",
    # "output_file":["/global/scratch/sd/sulsj/pythongrid-temp-dir/results.blast", "/global/scratch/sd/sulsj/pythongrid-temp-dir/usearch.log"],
    #  "cmd":"/project/projectdirs/genomes/sulsj/test/usearch-test/usearch32 --query /project/projectdirs/genomes/sulsj/test/2012.07.24-pythongrid-test/data/short.fna --db /global/scratch/sd/sulsj/pythongrid-temp-dir/nucl_input.fna --blastout /global/scratch/sd/sulsj/pythongrid-temp-dir/results.blast --blast6out /global/scratch/sd/sulsj/pythongrid-temp-dir/results.b6 -evalue 0.01  --log /global/scratch/sd/sulsj/pythongrid-temp-dir/usearch.log"}
]

# EOF