from pythongrid_cfg import CFG

def debugmsg(msg):
     
     import sys
     frame = sys._getframe(1)
     name = frame.f_code.co_name
     line_number = frame.f_lineno
     filename = frame.f_code.co_filename

     if (CFG['DEBUG']):
               return 'File "%s", line %d, in %s: \n>>> %s  ' % (filename, line_number, name, msg)
     else:
          return ""
     
# EOF
