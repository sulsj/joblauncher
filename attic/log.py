#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""
 

"""

import logging
import datetime
import os
from pythongrid_cfg import CFG


def setup_custom_logger(name):
    logger = logging.getLogger(name)
    
    if not logger.handlers:
        formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')
    
        # Set stream logger
        sh = logging.StreamHandler()
        sh.setFormatter(formatter)
    
        loglevel = CFG['LOG_LEVEL_STDOUT']
        numeric_level = getattr(logging, loglevel.upper(), None)
        if not isinstance(numeric_level, int):
            raise ValueError('Invalid log level: %s' % loglevel)
        logger.setLevel(numeric_level)
        logger.addHandler(sh)
        
        # Set file logger
        date_string = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
        log_file_name = (os.path.expanduser(CFG['TEMPDIR']) + 'joblauncher_' + date_string + ".log")
        fh = logging.FileHandler(log_file_name)
        fh.setFormatter(formatter)
        loglevel = CFG['LOG_LEVEL_FILE']
        numeric_level = getattr(logging, loglevel.upper(), None)
        if not isinstance(numeric_level, int):
            raise ValueError('Invalid log level: %s' % loglevel)
        fh.setLevel(loglevel)
        logger.addHandler(fh)
    
    return logger

# EOF