#!/bin/bash -l

SUBJECT=$1
MSG=$2
EMAILTO=$3
EMAILMESSAGE="/tmp/"$USER"_joblauncher_email.txt"
echo $EMAILMESSAGE
echo $MSG > $EMAILMESSAGE
/usr/bin/mail -s "$SUBJECT" "$EMAILTO" < $EMAILMESSAGE
ret=$?
exit $ret