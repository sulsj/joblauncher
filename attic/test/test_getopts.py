#!/usr/bin/python

import sys, getopt

def main(argv):
   inputfile = ''
   outputfile = ''
   sgeopt = ''
   try:
      opts, args = getopt.getopt(argv,"hi:o:p:",["ifile=","ofile=","sgeopt"])
   except getopt.GetoptError:
      print 'test.py -i <inputfile> -o <outputfile>'
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print 'test.py -i <inputfile> -o <outputfile>'
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputfile = arg
      elif opt in ("-o", "--ofile"):
         outputfile = arg
      elif opt in ("-p", "--sgeopt"):
         sgeopt = arg
   print 'Input file is "', inputfile
   print 'Output file is "', outputfile
   print 'SGE opts is ', sgeopt

if __name__ == "__main__":
   main(sys.argv[1:])
