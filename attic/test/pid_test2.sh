#!/bin/bash

/jgi/tools/bin/blastall -b 100 -v 100 -K 100 -p blastn -S 3 -d /global/scratch/sd/sulsj/eugene/BLASTDB/hs.m51.D4.diplotigs+fullDepthIsotigs.fa -e 1e-10 -F F -W 41 -i /global/scratch/sd/sulsj/eugene/makeflow-test-w-pe-slots/short.fna -m 8 -o test.bout &
PID2=$!

python ./zmq_test.py genepool04.nersc.gov 5002 1 $PID2 &
PID=$!

echo "zmq PID= $PID"
echo "blastll PID= $PID2"

wait $PID2 &&
kill -9 $PID
