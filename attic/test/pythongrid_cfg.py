"""configuration file for pythongrid"""
import os

# define a dict for configuration settings
CFG = {}

################################################################################
# default python path
################################################################################
#CFG['PYTHONPATH'] = os.environ['PYTHONPATH']
#CFG['PYGRID'] = "/project/projectdirs/genomes/sulsj/packages/genepool/x86_64-Linux/lib/python2.7/site-packages/pythongrid.py"
CFG['PYGRID'] = "/project/projectdirs/genomes/sulsj/test/2012.07.24-pythongrid-test/git-cloned/qsub-wrapper/pythongrid.py"
CFG['TEMPDIR'] = "/global/scratch/sd/sulsj/pythongrid-temp-dir/"
#CFG['TEMPDIR'] = "/project/projectdirs/genomes/sulsj/test/2012.07.24-pythongrid-test/temp/"
#CFG['node_blacklist'] = []

################################################################################
# to enable/disable debug messages
################################################################################
CFG['DEBUG'] = 1

################################################################################
# To pass the jobid and the master's host address to monitor the user process
################################################################################
CFG['JOBID'] = ""
CFG['HOMEADDR'] = ""

################################################################################
# error emails
################################################################################
CFG['SMTPSERVER'] = "smtp.gmail.com"
CFG['ERROR_MAIL_SENDER'] = "ssul@lbl.gov"
CFG['ERROR_MAIL_RECIPIENT'] = "sulsj0270@gmail.com"
CFG['MAX_MSG_LENGTH'] = 5000

################################################################################
# under the hood
################################################################################

# how much time can pass between heartbeats, before
# job is assummed to be dead in seconds
# In other words, if there is no heartbeat in 10s it is decided the node is dead.
CFG['MAX_TIME_BETWEEN_HEARTBEATS'] = 10

# factor by which to increase the requested memory
# if an out of memory event was detected.
# eq) compare (current mem usage)*1.20 with the requested mem 
CFG['MEM_THRESHOLD'] = 1.2
# If out-of-memory, increase 50% more of the failed request for both ram.c and h_vmem
CFG['OUT_OF_MEM_INCREASE'] = 1.5

# sulsj
# defines the wallclocktime threshold
# if the current runtime >= requeted wallclocktime * WALLCLOCKTIME_THRESH,
# then it's out of wallclocktime
CFG['WALLCLOCKTIME_THRESH'] = 0.9
CFG['OUT_OF_WALLCLOCKTIME_INCREASE'] = 1.25

# defines how many times can a particular job can die, 
# before we give up
#CFG['NUM_MAX_RESUBMITS'] = 10
# Categorized 
CFG['NUM_MAX_RESUBMITS_QSUB'] = 10
CFG['NUM_MAX_RESUBMITS_MEM'] = 10
CFG['NUM_MAX_RESUBMITS_WALLCLOCKTIME'] = 10
CFG['NUM_MAX_RESUBMITS_OUTPUT'] = 10

# check interval: how many seconds pass before we check
# on the status of a particular job in seconds
# This checking is from the master to compute node to check the submitted
# job is in qw, running, or done status
CFG['CHECK_FREQUENCY'] = 5

# heartbeat frequency: how many seconds pass before jobs
# on the cluster send back heart beats to the submission host
# HEARTBEAT_FREQUENCY: basic heartbeat
# PROCMON_FREQUENCY: heartbeat only for monitoring user process
CFG['HEARTBEAT_FREQUENCY'] = 5
CFG['PROCMON_FREQUENCY'] = 5

# qsub retry sleep time
# eq) if failed, sleep 10s and 20% increase for every retrial
CFG['QSUB_RETRY_SLEEP_TIME'] = 10
CFG['QSUB_RETRY_SLEEP_INCREASE'] = 2.0
CFG['OUTPUT_CHECK_SLEEP_TIME'] = 10
CFG['OUTPUT_CHECK_SLEEP_INCREASE'] = 1.0

# bulk job = job array 
#CFG['JOBARRAY'] = 0

# paths on cluster file system
# TODO: set this in configuration file

# location of pythongrid.py on cluster file system
# To do: set this in configuration file
#PPATH = reduce(lambda x,y: x+':'+y, PYTHONPATH)
##print debugmsg(PPATH)
#os.environ['PYTHONPATH'] = PPATH
#sys.path.extend(PYTHONPATH)
#import sys
#sys.path.append(os.getcwd())

# sulsj: currently not used
# white-list of nodes to use
CFG['WHITELIST'] = []
# black-list of nodes
CFG['BLACKLIST'] = []

if __name__ == '__main__':

    for key, value in CFG.items():
        print '#'*30
        print key, value
