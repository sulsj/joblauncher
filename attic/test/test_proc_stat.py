#!/usr/bin/env python

import sys
import os
import os.path 
#import datetime
import time

def _VmB(VmKey, pid):
    """
    get various mem usage properties of process with id pid in MB
    """

    _proc_status = '/proc/%d/status' % pid

    _scale = {'kB': 1.0/1024.0, 'mB': 1.0,
              'KB': 1.0/1024.0, 'MB': 1.0}

     # get pseudo file  /proc/<pid>/status
    try:
        t = open(_proc_status)
        v = t.read()
        t.close()
    except:
        return 0.0  # non-Linux?

    #print v

     # get VmKey line e.g. 'VmRSS:  9999  kB\n ...'
    i = v.index(VmKey)
    v = v[i:].split(None, 3)  # whitespace
    if len(v) < 3:
        return 0.0  # invalid format?
     # convert Vm value to bytes
    return float(v[1]) * _scale[v[2]]


def _runtime(pid):
    """
    get the total runtime in sec with pid.
    """
    _proc_stat = '/proc/%d/stat' % pid

    try:
        #now = datetime.datetime.now()
        #print "now1: ", now

        #now = time.time()
        #print "now2: ", int(now)

        #_date_f = os.popen('date "+%s"')
        #now = _date_f.read()
        #_date_f.close()
        #now = now.strip()
        #print "now3: ", now

        _proc_stat_f = os.popen('grep btime /proc/stat | cut -d " " -f 2')
        boottime = _proc_stat_f.read()
        _proc_stat_f.close()
        boottime = boottime.strip()

        _sec_since_boot_f = os.popen('cat /proc/$PID/stat | cut -d " " -f 22')
        p_seconds_since_boot = int(_sec_since_boot_f.read()) / 100
        _sec_since_boot_f.close()

        p_starttime = int(boottime) + p_seconds_since_boot
        now = time.time()
        p_runtime = int(now) - p_starttime

    except Exception, detail:
        print "getting runtime failed: ", detail
        boottime = "non-linux?"

    return p_runtime


def get_memory_usage(pid):
    """
    return memory usage in Mb.
    """

    return _VmB('VmSize:', pid)


def get_cpu_load(pid):
    """
    return cpu usage of process
    """

    command = "ps h -o pcpu -p %d" % (pid)

    try:
        ps_pseudofile = os.popen(command)
        info = ps_pseudofile.read()
        ps_pseudofile.close()

        cpu_load = info.strip()
    except Exception, detail:
        print "getting cpu info failed:", detail
        cpu_load = "non-linux?"

    return cpu_load



if __name__ == "__main__":

    pid = 4336
    print "mem usage: ", get_memory_usage(pid)
    print "cpu load: ", get_cpu_load(pid)
    print "run time: ", _runtime(pid)
