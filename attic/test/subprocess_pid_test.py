'''
Test subprocess termination
'''

import subprocess

command = 'cat'
command = '/jgi/tools/bin/blastall -b 100 -v 100 -K 100 -p blastn -S 3 -d /global/scratch/sd/sulsj/eugene/BLASTDB/hs.m51.D4.diplotigs+fullDepthIsotigs.fa -e 1e-10 -F F -W 41 -i /project/projectdirs/genomes/sulsj/test/2012.07.24-pythongrid-test/data/blast_query_1_8.fna -m 8 -o test1.m8.bout'

#keep pipes so that cat doesn't complain
proc = subprocess.Popen(command,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    stdin=subprocess.PIPE,
                    shell=True)

print('pid = %d' % proc.pid)
#subprocess.call("ps -A | grep -w %s" % command,
subprocess.call("ps -A | grep -w blastall",
                    shell=True)

#proc.terminate()
proc.wait()     
