#!/bin/bash

HZ="100" 
now=$(date "+%s")
PID=4336

boottime=$(grep btime /proc/stat | cut -d " " -f 2)

((p_seconds_since_boot = $(cat /proc/$PID/stat | cut -d " " -f 22) / $HZ ))

(( p_starttime = $boottime + $p_seconds_since_boot ))

(( p_runtime = $now - $p_starttime ))


echo -n "$msg: Process \"$process\" is running since "
echo "$p_runtime Seconds (Started: $(date -d @$p_starttime))"



