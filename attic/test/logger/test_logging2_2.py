# mylib.py
import logging

def do_something():
    import sys
    frame = sys._getframe(1)

    name = frame.f_code.co_name
    line_number = frame.f_lineno
    filename = frame.f_code.co_filename

    #s = name + " " + str(line_number) + " " + filename
    msg = ""
    s = "File '%s', line '%d', in '%s': >>> %s " % (filename, line_number, name, msg)
    
    logging.info(s + 'Doing something')
