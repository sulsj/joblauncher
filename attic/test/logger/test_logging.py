import logging
#logging.warning('Watch out!') # will print a message to the console
#logging.info('I told you so') # will not print anything
#logging.debug('This message should go to the log file')

logging.basicConfig(format='%(asctime)s %(message)s')
logging.basicConfig(filename='myapp.log', filemode='w', level=logging.DEBUG)

logging.debug('This message should go to the log file')
logging.info('I told you so') # will not print anything
logging.warning('Watch out!') # will print a message to the console
