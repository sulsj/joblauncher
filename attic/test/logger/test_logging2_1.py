# myapp.py
import logging
import test_logging2_2

def main():
    logging.basicConfig(format='%(levelname)s:%(message)s', filename='mylog.log', level=logging.DEBUG) 

    import sys
    frame = sys._getframe(1)

    name = frame.f_code.co_name
    line_number = frame.f_lineno
    filename = frame.f_code.co_filename

    #s = name + " " + str(line_number) + " " + filename
    msg = "test test test"
    s = "File '%s', line '%d', in '%s': >>> %s " % (filename, line_number, name, msg)

    logging.info(s + ' Started')
    test_logging2_2.do_something()
    logging.info(s +  'Finished')

    logging.debug(s + 'debug')



if __name__ == '__main__':
    main()
