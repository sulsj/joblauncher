#from pythongrid_cfg import CFG
import logging

module_logger = logging.getLogger('spam_application.debugmsg')


def debugmsg(msg):
     import sys
     frame = sys._getframe(1)
     name = frame.f_code.co_name
     line_number = frame.f_lineno
     filename = frame.f_code.co_filename
     s = "File '%s', line '%d', in '%s': \n>>> %s " % (filename, line_number, name, msg)

     if True:
     
          #import logging
          #logging.basicConfig(format='%(asctime)s %(message)s')  
          #logging.basicConfig(filename='myapp.log', level=logging.DEBUG)
          
          #logging.debug("[DEBUGMSG] File '%s', line '%d', in '%s': \n>>> %s " % (filename, line_number, name, msg))
     
          module_logger.info(s + ' from debugmsg')     

          
          return '[DEBUGMSG] File "%s", line %d, in %s: \n>>> %s  ' % (filename, line_number, name, msg)

     else:
          return ""
     
     
     
# EOF
