#!/bin/bash
#------------------------------------------------------------
# check_process_runtime.sh v. 0.0.1
# 
# simple nagios / icinga plugin
# to check the runtime of a process
# 
# The license is BSD.
# Use at your own risk!
# 31.03.2012 - F. Konietzny
#------------------------------------------------------------

test $# -lt 6 && {
        echo -e "\n\tUsage: $0 -p \"process string\" -c <N seconds> -w <N seconds> [ -s ] \n" >&2
        echo -e "\n\t-s\tsilent mode - dont raise an alert if the process is not running (optional)\n" >&2
        echo -e "\t-c\traise an critical alert if the runtime is more than N seconds\n" >&2
        echo -e "\t-w\traise an warning alert if the runtime is more than N seconds\n" >&2
        echo -e "\n\tExample: $0 -p pidgin -c 50000 -w 500 -s\n\n" >&2
	exit 4
}

silent=0

while getopts "sp:c:w:" opt; do
  case $opt in
    p)
                process="$OPTARG"
      ;;
    w)
                warn="$OPTARG"
      ;;
    c)
                crit="$OPTARG"
      ;;
    s)
            # critical when process is not found
            silent=1
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
     :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

PID=$(pgrep -oxf "$process")
test -z $PID && {
        if [ $silent -eq 0 ]; then
	    echo -e "\n\tError: \"$process\" not found\n\n"
	    exit 4
        else 
	    echo -e "\n\tOK: \"$process\" is not running\n\n"
	    exit 0
        fi
}

test -f /proc/stat || {
	echo -e "\n\tError: \"/proc/stat\" not found\n\n"
	exit 4
}

HZ="100" 
now=$(date "+%s")

boottime=$(grep btime /proc/stat | cut -d " " -f 2)

((p_seconds_since_boot = $(cat /proc/$PID/stat | cut -d " " -f 22) / $HZ ))

(( p_starttime = $boottime + $p_seconds_since_boot ))

(( p_runtime = $now - $p_starttime ))

if [ $p_runtime -gt $crit ]; then
    ret=2
    msg="CRITICAL"
    
elif [ $p_runtime -gt $warn ]; then
    ret=1
    msg="WARNING"
else
    ret=0
    msg="OK"
fi
echo -n "$msg: Process \"$process\" is running since "
echo "$p_runtime Seconds (Started: $(date -d @$p_starttime))"
exit $ret
#------------------------------------------------------------
# eof
#------------------------------------------------------------
