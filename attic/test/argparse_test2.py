import argparse
import sys

def main(argv=None):
    desc = u'joblauncher'
    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument('-i', '--input', help='Task list file name.', dest='file_name')
    parser.add_argument('-l', '--log', help='Log file name.', dest='log_file_name')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s 0.9.0')

    args = parser.parse_args()
    print args
    print args.file_name


if __name__ == "__main__":
    sys.exit(main())
