#!/bin/sh

module load python
#./joblauncher.py --name test_usearch --ram_c 2G --h_vmem 2G --h_rt 00:05:00 --sge_param ' -j y -shell y -S /bin/bash -w e -cwd -l debug.c -pe pe_slots 8 ' --od ./out-usearch --cf 'results.blast,usearch.log' --cmd './usearch32 --query ./data/short.fna --db ./data/nucl_input.fna --blastout ./out-usearch/results.blast --blast6out ./out-usearch/results.b6 -evalue 0.01  --log ./out-usearch/usearch.log' --retry 3 --delay 10
JOBLAUNCHER=`pwd`/joblauncher.py
# $JOBLAUNCHER --name test_usearch --ram_c 5G --h_vmem 5G --h_rt 00:30:00 --sge_param ' -j y -shell y -S /bin/bash -w e -cwd -pe pe_1 1  ' --od ./out-usearch --cf 'results.blast,usearch.log' --cmd './usearch32 --query ./data/short.fna --db ./data/nucl_input.fna --blastout ./out-usearch/results.blast --blast6out ./out-usearch/results.b6 -evalue 0.01  --log ./out-usearch/usearch.log' --retry 3 --delay 10
$JOBLAUNCHER --name usearch_test1 --ramc 5G --hvmem 5G --hrt 00:30:00 --sgeparam ' -j y -shell y -S /bin/bash -w e -cwd -pe pe_1 1 -l high.c ' --checkfile './out-usearch/results.blast,./out-usearch/usearch.log' --cmd './usearch32 --query ./data/short.fna --db ./data/nucl_input.fna --blastout ./out-usearch/results.blast --blast6out ./out-usearch/results.b6 -evalue 0.01  --log ./out-usearch/usearch.log' --retry 3 --delay 10 
