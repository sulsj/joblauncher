#!/bin/bash

echo "Start blastall"
/jgi/tools/bin/blastall -b 100 -v 100 -K 100 -p blastn -S 3 -d ../data/hs.m51.D4.diplotigs+fullDepthIsotigs.fa -e 1e-10 -F F -W 41 -i ../data/blast_query_1_160.fna -m 8 -o /global/scratch/sd/sulsj/pythongrid-temp-dir/test1.m8.bout
