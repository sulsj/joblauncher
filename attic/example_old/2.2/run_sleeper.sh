#!/bin/sh

module load python
JOBLAUNCHER=`pwd`/joblauncher.py
# $JOBLAUNCHER --name sleep_test1 --ram_c 4G --h_vmem 4G --h_rt 00:30:00 --sge_param ' -j y -shell y -S /bin/bash -w e -cwd -l high.c -pe pe_slots 1 ' --od ./out-sleep --cf test1.sleep --cmd '/house/homedirs/a/andreopo/python/drmaa-0.5/sleeper.sh ABC DEF' --retry 3 --delay 10
$JOBLAUNCHER --name sleep_test1 --ramc 4G --hvmem 4G --hrt 00:30:00 --sgeparam ' -j y -shell y -S /bin/bash -w e -cwd -l high.c -pe pe_slots 1 '  --checkfile ./out-sleep/test1.sleep --cmd '/house/homedirs/a/andreopo/python/drmaa-0.5/sleeper.sh ABC DEF > ./out-sleep/test1.sleep 2>&1 ' --retry 3 --delay 10 