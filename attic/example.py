#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""
 

"""

from pythongrid_genepool import GenepoolJob, process_jobs
import time

### sulsj
from __debugmsg__ import debugmsg
import subprocess
import os


def compute_factorial(n):
    """
    computes factorial of n
    """

    time.sleep(5)
    
    retval = 1
    for i in xrange(n):
        retval=retval*(i+1)

    return retval


def run_something(cmd):
	"""
	run any command in the task[cmd] part

	@args list including command + opts
	"""
	
	time.sleep(1)
	
	print debugmsg("cmd to run = " + cmd)
	p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
	#print debugmsg("pid = " + str(p.pid))
	#p2 = subprocess.Popen("ps -A | grep -w blastall", shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
	#stdout_value2 = p2.communicate()[0]
	#retval2 = p2.wait()
	#print debugmsg("[XXX]" + stdout_value2)

	stdout_value = p.communicate()[0]
	retval = p.wait()

	print debugmsg("stdout_value from cmd = " + stdout_value)
	print debugmsg("cmd is done!")

	return retval


def make_jobs():
    """
    creates a list of Genepool Job objects, which carry all information needed
    for a function to be executed on SGE:
    - function object
    - arguments
    - settings
    """

    # set up list of arguments
	# TODO: Tasks should be taken from user's command line argument
	
	#"""
	#	taskList = [
	##				{"ram_c":"1G", 
	##      			 "h_vmem":"1G",
	##    			 "sge_param":" -N test1 -S /bin/bash -w e -cwd -l debug.c -l h_rt=00:10:00 -l ram.c=2G -pe pe_slots 1 -P system.p",
	##    			 "cmd":"/jgi/tools/bin/blastall -b 100 -v 100 -K 100 -p blastn -S 3 -d /global/scratch/sd/sulsj/eugene/BLASTDB/hs.m51.D4.diplotigs+fullDepthIsotigs.fa -e 1e-10 -F F -W 41 -i /project/projectdirs/genomes/sulsj/test/2012.07.24-pythongrid-test/long.fna -m 8 -o test1.m8.bout",
	##    			 "output":"test1.m8.bout"},
	#				 {"name":"test2",
	#				  "ram_c":"2G",
	#				  "h_vmem":"2G",
	#				  "h_rt":"00:10:00",
	#				  "sge_param":"-shell y -N test2 -S /bin/bash -w e -cwd -l debug.c -pe pe_slots 1 -P system.p",
	#				  "cmd":"/jgi/tools/bin/blastall -b 100 -v 100 -K 100 -p blastn -S 3 -d /global/scratch/sd/sulsj/eugene/BLASTDB/hs.m51.D4.diplotigs+fullDepthIsotigs.fa -e 1e-10 -F F -W 41 -i /global/scratch/sd/sulsj/eugene/makeflow-test-w-pe-slots/short.fna -m 8 -o test2.m8.bout",
	#				  "output":"test2.m8.bout"}
	#				]
	#	#print debugmsg(taskList)
	#"""

    # create empty job vector
    jobs=[]
	
    # create job objects
    #for aTask in taskList:
    #    print debugmsg(aTask["cmd"])
    #    job = GenepoolJob(run_something, aTask)
    #    jobs.append(job)
    #inputvec = [[3]] 
    #inputvec = [["/jgi/tools/bin/blastall -b 100 -v 100 -K 100 -p blastn -S 3 -d /global/scratch/sd/sulsj/eugene/BLASTDB/hs.m51.D4.diplotigs+fullDepthIsotigs.fa -e 1e-10 -F F -W 41 -i /project/projectdirs/genomes/sulsj/test/2012.07.24-pythongrid-test/data/long.fna -m 8 -o test1.m8.bout"]]
    inputvec = [["/jgi/tools/bin/blastall -b 100 -v 100 -K 100 -p blastn -S 3 -d /global/scratch/sd/sulsj/eugene/BLASTDB/hs.m51.D4.diplotigs+fullDepthIsotigs.fa -e 1e-10 -F F -W 41 -i /project/projectdirs/genomes/sulsj/test/2012.07.24-pythongrid-test/data/blast_query_1_160.fna -m 8 -o test1.m8.bout"]]

    for arg in inputvec:
        #job = GenepoolJob(compute_factorial, arg) 
        job = GenepoolJob(run_something, arg)
        job.name = "test1"
        job.h_vmem = "2000M"
        job.ram_c = "2000M"
        job.h_rt = "00:10:00"
        #job.sge_param=" -j y -shell y -S /bin/bash -w e -cwd -l debug.c -pe pe_slots 1 -P system.p"
        job.sge_param=" -j y -shell y -S /bin/bash -w e -cwd -l debug.c -pe pe_slots 1 "
        jobs.append(job)
    
    
    return jobs


def run_on_cluster():
    """
    run a set of jobs on cluster
    """
    
    print ""
    print ""
    print "====================================="
    print "========   Submit and Wait   ========"
    print "====================================="
    print ""
    print ""

    functionJobs = make_jobs()

    print ""
    print "sending function jobs to cluster"
    print ""

    processedFunctionJobs = process_jobs(functionJobs)

    print "ret fields AFTER execution on cluster"
    for (i, job) in enumerate(processedFunctionJobs):
        print "Job #", i, "- ret: ", str(job.ret)



def main(argv=None):
    """
    main function to set up example
    """
 
    # execute function on the cluster
    run_on_cluster()


if __name__ == "__main__":
    main()

# EOF
