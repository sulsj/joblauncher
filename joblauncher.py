#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Written (W) 2012-2013 Seung-Jin Sul
# Copyright (C) NERSC, LBL

"""
A robust qsub wrapper for general purpose qsub replacement.

This targets at developing a backbone implementation of workflow management
system. It provides users with an unified way to submit their jobs and to check
output files for synchronization. At JGI, there are many different ways to do
that.

"""

__version__ = "2.3"
  
import sys
import time
import multiprocessing
import subprocess
import socket
import argparse
import datetime

from joblauncher_cfg import CFG
import pythongrid_nersc
from pythongrid import heart_beat
from Utils.OsUtility import *

import logging
logger = logging.getLogger('root')

## To hide "No handlers could be found for logger "pika.adapters.base_connection"" warning
#logging.getLogger('pika').setLevel(logging.INFO)

    
"""
Run user command
This will be passed to "apply" in pythongrid.py and
ran on a compute node.

@param cmd: list including command + opts
"""
#------------------------------------------------------------------------------
def runUserCmd(cmd):
#------------------------------------------------------------------------------
	print "\nUser command: " + cmd
	p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
	print "pid = %s on the host = %s" % (str(p.pid), socket.gethostname())

	jobId = CFG['JOBID']
	address = CFG['HOMEADDR']
	monitorProc = multiprocessing.Process(target=heart_beat,
                                      args=(jobId, address,
                                            "proc_mon",
                                            "runUserCmd() on compute node",
                                            p.pid, "",
                                            CFG["PROCMON_FREQUENCY"]))
	monitorProc.start()

	## sulsj
	# For debug
	# This is to make sure that the child process id from multiprocessing.Process is
	# matched with the user command to run

	#p2 = subprocess.Popen("ps -A | grep -w blastall",
                            #shell=True,
                            #stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                            #close_fds=True)
	#stdout_value2 = p2.communicate()[0]
	#retval2 = p2.wait()
	#print "stdout_value2 = " + stdout_value2

	stdoutValue = p.communicate()[0]
	p.wait()
	p.poll()
	retVal = p.returncode
	print "stdout from cmd = %s" % (stdoutValue)
	print "return code from cmd = %s" % (retVal)

	monitorProc.terminate()

	return retVal

"""
Create job objects and append to jobs list
"""
#------------------------------------------------------------------------------
def makeJobs(args):
#------------------------------------------------------------------------------
    ## To do
    ## Load user task list from input file by "-i"
    
	jobs=[]
	arg = []
	arg.append(args.cmd)

	outFileList = []
	if args.outputFiles:
	    for t in args.outputFiles.split(","):
		    outFileList.append(t)

	logger.info("Output files to check: %s", outFileList)

	## For debugging
	job = pythongrid_nersc.NerscJob(runUserCmd, arg)
	job.name 		= args.jobName
	job.h_vmem 	   	= args.h_vmem
	job.ram_c       = args.ram_c
	job.h_rt 		= args.h_rt
	job.output_file = outFileList
	job.sge_param 	= args.sgeParams
	job.user_cmd   	= args.cmd

	if args.numRetrials:
		CFG['NUM_MAX_RESUBMITS_QSUB'] = args.numRetrials
	if args.delayTime:
		CFG['QSUB_RETRY_SLEEP_TIME'] = args.delayTime
		CFG['OUTPUT_CHECK_SLEEP_TIME'] = args.delayTime
        
	jobs.append(job)

	return jobs

"""
Setting up logging

@param loglevel: logger level (string: info, debug, warning...)
"""
#------------------------------------------------------------------------------
def setupCustomLogger(loglevel):
#------------------------------------------------------------------------------
    numericLogLevel = getattr(logging, loglevel.upper(), None)
    if not isinstance(numericLogLevel, int):
        raise ValueError('Invalid log level: %s' % args.logLevel)
    
    dateString = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    logFileName = os.path.expanduser(CFG['LOGDIR']) + '/joblauncher_' + dateString + ".log"

    formatter = logging.Formatter('%(asctime)s | %(module)s | %(levelname)s : %(message)s')
    logger.setLevel(logging.DEBUG)

    streamLogger = logging.StreamHandler()
    streamLogger.setLevel(numericLogLevel)
    streamLogger.setFormatter(formatter)

    logFilePath = logFileName
    fileLogger = logging.FileHandler(logFilePath)
    fileLogger.setFormatter(formatter)
    fileLogger.setLevel(numericLogLevel)

    logger.addHandler(fileLogger)
    logger.addHandler(streamLogger)

    logger.info("Log file name: %s" % (logFileName))

    return logger


"""
run a set of jobs on cluster
"""
#------------------------------------------------------------------------------
def runOnCluster(args):
#------------------------------------------------------------------------------
    logger.info('=====================================')
    logger.info(' Submit and Wait ')
    logger.info('=====================================')

    userJobs = makeJobs(args)

    logger.info("Sending job(s) to cluster")
    processedFunctionJobs = pythongrid_nersc.processJobs(userJobs)

    logger.info( "=====================================")
    logger.info( " Summary")
    logger.info( "=====================================")

    for (i, job) in enumerate(processedFunctionJobs):
        logger.info("Job#: %d - ret: %s" % (i, str(job.ret)))
        logger.info("Job name, jobid: %s, %s" % (str(job.name), str(job.jobid)))
        logger.info("User command: %s", str(job.user_cmd))
        logger.info("Output file(s): %s", str(job.output_file))
        logger.info("SGE params: ram.c=%s, h_vmem=%s, h_rt=%s, other_param=%s" % (job.ram_c, job.h_vmem, job.h_rt, job.sge_param))
        logger.info("Last compute node name: %s", str(job.host_name))
        logger.info("Last timestamp: %s", str(job.timestamp))
        if job.ret == "job dead":
			logger.info("Cause of death: %s", job.cause_of_death)
			job.track_procmon_runtime = 0
        logger.info("Number of total retrials: %s" % (job.num_total_resubmits))
        logger.info("   Number of qsub retrials: %s" % (job.num_resubmits_qsub))
        logger.info("   Number of out-of-mem retrials: %s" % (job.num_resubmits_mem))
        logger.info("   Number of out-of-wallclocktime retrials: %s" % (job.num_resubmits_wallclock))
        logger.info("   Number of output-file-error retrials: %s" % (job.num_resubmits_output))

        logger.info("Queue wait time: ")
        for i in range(len(job.qw_start)):
			logger.info("%s ", str(job.qw_end[i] - job.qw_start[i]))
        #logger.info("Python's mem usage: %s" % (job.track_mem)
        #logger.info("Python's cpu usage: %s" % (job.track_cpu)
        #logger.info("Python's runtime: %s seconds" % (job.track_runtime)
        logger.info("User command's mem usage: %s" % (job.track_procmon_mem))
        logger.info("User command's cpu usage: %s" % (job.track_procmon_cpu))
        logger.info("User command's runtime: %s seconds" % (job.track_procmon_runtime))
        
"""
main function to set up example
"""
#------------------------------------------------------------------------------
def main(argv=None):
#------------------------------------------------------------------------------
    desc = u'joblauncher'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-c', '--checkfile',
                        help='list of output file(s) to check (comma separated)',
                        dest='outputFiles', required=False)
    parser.add_argument('-e', '--hvmem',
                        help='h_vmem (GB)',
                        dest='h_vmem', required=True)
    parser.add_argument('-i', '--taskfile',
                        help='input task list file',
                        dest='inputFile', required=False)
    parser.add_argument('-m', '--ramc',
                        help='ram.c (GB)',
                        dest='ram_c', required=True)
    parser.add_argument('-l', '--loglevel',
                        help='loglevel (default=info)',
                        dest='logLevel',
                        required=False,
                        default="info")
    parser.add_argument('-n', '--name',
                        help='job name',
                        dest='jobName', required=True)
    parser.add_argument('-p', '--sgeparam',
                        help='sge parameters',
                        dest='sgeParams', required=True)
    parser.add_argument('-r', '--retry',
                        help='max. number of resubmissions for invalid jobid or output files',
                        dest='numRetrials', required=False)
    parser.add_argument('-t', '--hrt',
                        help='h_rt (hh:mm:ss)',
                        dest='h_rt', required=True)
    parser.add_argument('-u', '--cmd',
                        help='program to run',
                        dest='cmd', required=True)
    parser.add_argument('-y', '--tmpdir',
                        help='temp directory',
                        dest='tempDir', required=False, default="tmp")
    parser.add_argument('-z', '--logdir',
                        help='log directory',
                        dest='logDir', required=False, default="log")
    parser.add_argument('-v', '--version', action='version', version=__version__)
    parser.add_argument('-w', '--delay',
                        help='wait time in sec between retrials',
                        dest='delayTime', required=False)
    args = parser.parse_args()

    if args.tempDir: CFG['TEMPDIR'] = args.tempDir
    if args.logDir:  CFG['LOGDIR'] = args.logDir

    makeDir(CFG['TEMPDIR'])
    makeDir(CFG['LOGDIR'])
    
    ## setup logger
    logger = setupCustomLogger(args.logLevel)
        
    ## execute function on the cluster
    runOnCluster(args)

	## To do: if the web-based reporting was enabled, check this.
    #time.sleep(10) # for web page
    #if CFG['CHERRY_PID']:
		#os.kill(CFG['CHERRY_PID'], signal.SIGKILL)

    ## To do
    ## capture Ctrl+C and terminate job
    

if __name__ == "__main__":
    sys.exit(main())
    
# EOF
